#!/bin/bash

function releasePatch(){
cd src/core/theme
npm version patch
cd ../
cd reactjs
npm version patch
cd ../
cd react-native
npm version patch
cd ../../react-sketchapp
npm version patch
cd ../../
git add .
git commit -m 'New patch release'
npm version patch
}
releasePatch
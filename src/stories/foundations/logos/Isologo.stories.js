import React from 'react';
import { CRLogo } from '../../../core/components/CRLogo';

export default {
  title: '🧱 Foundations/©️ Logos/🔤 Isologo',
  argTypes: {
    children: { control: { disable: true } },
    className: { control: { disable: true } },
  },
  parameters: {
    controls: { disabled: true },
    docs: {
      description: {
        component: 'Imagotype',
      },
    },
  },
};

const Comp = (args) => <CRLogo.Main {...args} />;
const CompNegative = (args) => <CRLogo.Negative {...args} />;

export const Main = Comp.bind({});
export const Negative = CompNegative.bind({});
Negative.decorators = [
  (Story) => (
    <div className={'mijo--darkCard'}>
      <Story />
    </div>
  ),
];

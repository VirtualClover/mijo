import React from "react";
//import docs from "../../../../about-docs/foundations/colors/primaries/dark.md";
import { CRLogo } from "../../../core/components/CRLogo";

export default {
  title: "🧱 Foundations/©️ Logos/🖼️ Isotype",
  argTypes: {
    children: { control: { disable: true } },
    className: { control: { disable: true } },
  },
  parameters: {
    controls: { disabled: true },
    docs: {
      description: {
        component: "Isotype",
      },
    },
  },
};

const Comp = (args) => <CRLogo.Isotype {...args} />;
const CompNegative = (args) => <CRLogo.IsotypeNegative {...args} />;

export const Main = Comp.bind({});
export const Negative = CompNegative.bind({});
Negative.decorators = [
  (Story) => (
    <div className={"mijo--darkCard"}>
      <Story />
    </div>
  ),
];

import React from 'react';
import { CRText } from '@mijo/components/CRText';
import docsTitle from '../../../about-docs/foundations/typography/title.md';
import docsSubtitle from '../../../about-docs/foundations/typography/subtitle.md';
import docsSubtitle2 from '../../../about-docs/foundations/typography/subtitle2.md';
import docsSubtitle3 from '../../../about-docs/foundations/typography/subtitle3.md';
import docsParagraph from '../../../about-docs/foundations/typography/paragraph.md';
import docsCaption from '../../../about-docs/foundations/typography/caption.md';
import docsBold from '../../../about-docs/foundations/typography/bold.md';

export default {
  title: '🧱 Foundations/📜 Typography/🔠 Text Styles',
  argTypes: {
    variant: { control: { disable: true } },
  },
  parameters: {
    docs: {
      description: {
        component: '',
      },
    },
  },
};

const Comp = (args) => <CRText {...args} />;

export const Title = Comp.bind({});
Title.args = {
  variant: 'title',
  children: 'This is a title.',
};
Title.parameters = {
  docs: {
    description: {
      component: docsTitle,
    },
    source: {
      code: `<CRText variant={'title'} />`,
    },
  },
};

export const Subtitle = Comp.bind({});
Subtitle.args = {
  variant: 'subtitle',
  children: 'This is a subtitle.',
};
Subtitle.parameters = {
  docs: {
    description: {
      component: docsSubtitle,
    },
    source: {
      code: `<CRText variant={'subtitle'} />`,
    },
  },
};

export const Subtitle2 = Comp.bind({});
Subtitle2.args = {
  variant: 'subtitle2',
  children: 'This is a smaller subtitle.',
};
Subtitle2.parameters = {
  docs: {
    description: {
      component: docsSubtitle2,
    },
    source: {
      code: `<CRText variant={'subtitle2'} />`,
    },
  },
};

export const Subtitle3 = Comp.bind({});
Subtitle3.args = {
  variant: 'subtitle3',
  children: 'This is the smallest subtitle.',
};
Subtitle3.parameters = {
  docs: {
    description: {
      component: docsSubtitle3,
    },
    source: {
      code: `<CRText variant={'subtitle3'} />`,
    },
  },
};

export const Paragraph = Comp.bind({});
Paragraph.args = {
  variant: 'paragraph',
  children: 'This is a paragraph.',
};
Paragraph.parameters = {
  docs: {
    description: {
      component: docsParagraph,
    },
    source: {
      code: `<CRText variant={'paragraph'} />`,
    },
  },
};

export const Caption = Comp.bind({});
Caption.args = {
  variant: 'caption',
  children: 'This is a caption.',
};
Caption.parameters = {
  docs: {
    description: {
      component: docsCaption,
    },
    source: {
      code: `<CRText variant={'caption'} />`,
    },
  },
};

export const Micro = Comp.bind({});
Micro.args = {
  variant: 'micro',
  children: 'This is some micro text.',
};
Micro.parameters = {
  docs: {
    source: {
      code: `<CRText variant={'micro'} />`,
    },
  },
};

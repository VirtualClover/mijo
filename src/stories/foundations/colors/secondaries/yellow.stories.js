import React from "react";
import { CRView } from "@mijo/components/CRView";
import { CRText } from "@mijo/components/CRText";
import { colors } from "@mijo/theme/colors";
import docs from "../../../../about-docs/foundations/colors/secondaries/yellow.md";

export default {
  title: "🧱 Foundations/🌈 Colors/Secondaries/Yellow",
  argTypes: {
    children: { control: { disable: true } },
    className: { control: { disable: true } },
  },
  parameters: {
    controls: { disabled: true },
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRView {...args} />;

export const Yellow100 = Comp.bind({});
Yellow100.args = {
  className: "cr-bgc-yellow100 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant="subtitle2">
        Yellow 100
      </CRText>
      <CRText align={"center"}>{colors.yellow100}</CRText>
    </CRView>
  ),
};
Yellow100.parameters = {
  backgrounds: {
    default: "Yellow100",
    values: [
      {
        name: "Yellow100",
        value: colors.yellow100,
      },
    ],
  },
};

export const Yellow200 = Comp.bind({});
Yellow200.args = {
  className: "cr-bgc-yellow200 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"}>
        Yellow 200
      </CRText>
      <CRText align={"center"}>{colors.yellow200}</CRText>
    </CRView>
  ),
};
Yellow200.parameters = {
  backgrounds: {
    default: "Yellow200",
    values: [
      {
        name: "Yellow200",
        value: colors.yellow200,
      },
    ],
  },
};

export const Yellow300 = Comp.bind({});
Yellow300.args = {
  className:
    "cr-bgc-yellow300 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"}>
        Yellow 300
      </CRText>
      <CRText align={"center"}>{colors.yellow300}</CRText>
    </CRView>
  ),
};
Yellow300.parameters = {
  backgrounds: {
    default: "Yellow300",
    values: [
      {
        name: "Yellow300",
        value: colors.yellow300,
      },
    ],
  },
};

export const Yellow400 = Comp.bind({});
Yellow400.args = {
  className:
    "cr-bgc-yellow400 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Yellow 400
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.yellow400}
      </CRText>
    </CRView>
  ),
};
Yellow400.parameters = {
  backgrounds: {
    default: "Yellow400",
    values: [
      {
        name: "Yellow400",
        value: colors.yellow400,
      },
    ],
  },
};

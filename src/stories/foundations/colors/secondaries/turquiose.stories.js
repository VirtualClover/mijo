import React from "react";
import { CRView } from "@mijo/components/CRView";
import { CRText } from "@mijo/components/CRText";
import { colors } from "@mijo/theme/colors";
import docs from "../../../../about-docs/foundations/colors/secondaries/turquiose.md";

export default {
  title: "🧱 Foundations/🌈 Colors/Secondaries/Turquiose",
  argTypes: {
    children: { control: { disable: true } },
    className: { control: { disable: true } },
  },
  parameters: {
    controls: { disabled: true },
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRView {...args} />;

export const Turquiose100 = Comp.bind({});
Turquiose100.args = {
  className: "cr-bgc-turquiose100 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant="subtitle2">
        Turquiose 100
      </CRText>
      <CRText align={"center"}>{colors.turquiose100}</CRText>
    </CRView>
  ),
};
Turquiose100.parameters = {
  backgrounds: {
    default: "Turquiose100",
    values: [
      {
        name: "Turquiose100",
        value: colors.turquiose100,
      },
    ],
  },
};

export const Turquiose200 = Comp.bind({});
Turquiose200.args = {
  className: "cr-bgc-turquiose200 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"}>
        Turquiose 200
      </CRText>
      <CRText align={"center"}>{colors.turquiose200}</CRText>
    </CRView>
  ),
};
Turquiose200.parameters = {
  backgrounds: {
    default: "Turquiose200",
    values: [
      {
        name: "Turquiose200",
        value: colors.turquiose200,
      },
    ],
  },
};

export const Turquiose300 = Comp.bind({});
Turquiose300.args = {
  className:
    "cr-bgc-turquiose300 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Turquiose 300
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.turquiose300}
      </CRText>
    </CRView>
  ),
};
Turquiose300.parameters = {
  backgrounds: {
    default: "Turquiose300",
    values: [
      {
        name: "Turquiose300",
        value: colors.turquiose300,
      },
    ],
  },
};

export const Turquiose400 = Comp.bind({});
Turquiose400.args = {
  className:
    "cr-bgc-turquiose400 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Turquiose 400
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.turquiose400}
      </CRText>
    </CRView>
  ),
};
Turquiose400.parameters = {
  backgrounds: {
    default: "Turquiose400",
    values: [
      {
        name: "Turquiose400",
        value: colors.turquiose400,
      },
    ],
  },
};

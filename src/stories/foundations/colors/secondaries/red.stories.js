import React from "react";
import { CRView } from "@mijo/components/CRView";
import { CRText } from "@mijo/components/CRText";
import { colors } from "@mijo/theme/colors";
import docs from "../../../../about-docs/foundations/colors/secondaries/red.md";

export default {
  title: "🧱 Foundations/🌈 Colors/Secondaries/Red",
  argTypes: {
    children: { control: { disable: true } },
    className: { control: { disable: true } },
  },
  parameters: {
    controls: { disabled: true },
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRView {...args} />;

export const Red100 = Comp.bind({});
Red100.args = {
  className: "cr-bgc-red100 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant="subtitle2">
        Red 100
      </CRText>
      <CRText align={"center"}>{colors.red100}</CRText>
    </CRView>
  ),
};
Red100.parameters = {
  backgrounds: {
    default: "Red100",
    values: [
      {
        name: "Red100",
        value: colors.red100,
      },
    ],
  },
};

export const Red200 = Comp.bind({});
Red200.args = {
  className: "cr-bgc-red200 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Red 200
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.red200}
      </CRText>
    </CRView>
  ),
};
Red200.parameters = {
  backgrounds: {
    default: "Red200",
    values: [
      {
        name: "Red200",
        value: colors.red200,
      },
    ],
  },
};

export const Red300 = Comp.bind({});
Red300.args = {
  className:
    "cr-bgc-red300 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Red 300
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.red300}
      </CRText>
    </CRView>
  ),
};
Red300.parameters = {
  backgrounds: {
    default: "Red300",
    values: [
      {
        name: "Red300",
        value: colors.red300,
      },
    ],
  },
};

export const Red400 = Comp.bind({});
Red400.args = {
  className:
    "cr-bgc-red400 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Red 400
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.red400}
      </CRText>
    </CRView>
  ),
};
Red400.parameters = {
  backgrounds: {
    default: "Red400",
    values: [
      {
        name: "Red400",
        value: colors.red400,
      },
    ],
  },
};

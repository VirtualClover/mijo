import React from "react";
import { CRView } from "@mijo/components/CRView";
import { CRText } from "@mijo/components/CRText";
import { colors } from "@mijo/theme/colors";
import docs from "../../../../about-docs/foundations/colors/primaries/deep.md";

export default {
  title: "🧱 Foundations/🌈 Colors/Primaries/Deep",
  argTypes: {
    children: { control: { disable: true } },
    className: { control: { disable: true } },
  },
  parameters: {
    controls: { disabled: true },
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRView {...args} />;

export const Deep100 = Comp.bind({});
Deep100.args = {
  className: "cr-bgc-deep100 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} color={"white"} variant="subtitle2">
        Deep 100
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.deep100}
      </CRText>
    </CRView>
  ),
};
Deep100.parameters = {
  backgrounds: {
    default: "Deep100",
    values: [
      {
        name: "Deep100",
        value: colors.deep100,
      },
    ],
  },
};

export const Deep200 = Comp.bind({});
Deep200.args = {
  className: "cr-bgc-deep200 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Deep 200
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.deep200}
      </CRText>
    </CRView>
  ),
};
Deep200.parameters = {
  backgrounds: {
    default: "Deep200",
    values: [
      {
        name: "Deep200",
        value: colors.deep200,
      },
    ],
  },
};

export const Deep300 = Comp.bind({});
Deep300.args = {
  className:
    "cr-bgc-deep300 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Deep 300
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.deep300}
      </CRText>
    </CRView>
  ),
};
Deep300.parameters = {
  backgrounds: {
    default: "Deep300",
    values: [
      {
        name: "Deep300",
        value: colors.deep300,
      },
    ],
  },
};

export const Deep400 = Comp.bind({});
Deep400.args = {
  className:
    "cr-bgc-deep400 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Deep 400
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.deep400}
      </CRText>
    </CRView>
  ),
};
Deep400.parameters = {
  backgrounds: {
    default: "Deep400",
    values: [
      {
        name: "Deep400",
        value: colors.deep400,
      },
    ],
  },
};

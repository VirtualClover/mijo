import React from "react";
import { CRView } from "@mijo/components/CRView";
import { CRText } from "@mijo/components/CRText";
import { colors } from "@mijo/theme/colors";
import docs from "../../../../about-docs/foundations/colors/primaries/main.md";

export default {
  title: "🧱 Foundations/🌈 Colors/Primaries/Main",
  argTypes: {
    children: { control: { disable: true } },
    className: { control: { disable: true } },
  },
  parameters: {
    controls: { disabled: true },
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRView {...args} />;

export const Main100 = Comp.bind({});
Main100.args = {
  className: "cr-bgc-main100 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant="subtitle2">
        Main 100
      </CRText>
      <CRText align={"center"}>{colors.main100}</CRText>
    </CRView>
  ),
};
Main100.parameters = {
  backgrounds: {
    default: "Main100",
    values: [
      {
        name: "Main100",
        value: colors.main100,
      },
    ],
  },
};

export const Main200 = Comp.bind({});
Main200.args = {
  className: "cr-bgc-main200 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Main 200
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.main200}
      </CRText>
    </CRView>
  ),
};
Main200.parameters = {
  backgrounds: {
    default: "Main200",
    values: [
      {
        name: "Main200",
        value: colors.main200,
      },
    ],
  },
};

export const Main300 = Comp.bind({});
Main300.args = {
  className:
    "cr-bgc-main300 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Main 300
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.main300}
      </CRText>
    </CRView>
  ),
};
Main300.parameters = {
  backgrounds: {
    default: "Main300",
    values: [
      {
        name: "Main300",
        value: colors.main300,
      },
    ],
  },
};

export const Main400 = Comp.bind({});
Main400.args = {
  className:
    "cr-bgc-main400 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Main 400
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.main400}
      </CRText>
    </CRView>
  ),
};
Main400.parameters = {
  backgrounds: {
    default: "Main400",
    values: [
      {
        name: "Main400",
        value: colors.main400,
      },
    ],
  },
};

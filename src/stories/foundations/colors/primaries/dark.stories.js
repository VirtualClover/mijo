import React from "react";
import { CRView } from "@mijo/components/CRView";
import { CRText } from "@mijo/components/CRText";
import { colors } from "@mijo/theme/colors";
import docs from "../../../../about-docs/foundations/colors/primaries/dark.md";

export default {
  title: "🧱 Foundations/🌈 Colors/Primaries/Dark",
  argTypes: {
    children: { control: { disable: true } },
    className: { control: { disable: true } },
  },
  parameters: {
    controls: { disabled: true },
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRView {...args} />;

export const Dark100 = Comp.bind({});
Dark100.args = {
  className: "cr-bgc-dark100 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant="subtitle2" color={"white"}>
        Dark 100
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.dark100}
      </CRText>
    </CRView>
  ),
};
Dark100.parameters = {
  backgrounds: {
    default: "Dark100",
    values: [
      {
        name: "Dark100",
        value: colors.dark100,
      },
    ],
  },
};

export const Dark200 = Comp.bind({});
Dark200.args = {
  className: "cr-bgc-dark200 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Dark 200
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.dark200}
      </CRText>
    </CRView>
  ),
};
Dark200.parameters = {
  backgrounds: {
    default: "Dark200",
    values: [
      {
        name: "Dark200",
        value: colors.dark200,
      },
    ],
  },
};

export const Dark300 = Comp.bind({});
Dark300.args = {
  className:
    "cr-bgc-dark300 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Dark 300
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.dark300}
      </CRText>
    </CRView>
  ),
};
Dark300.parameters = {
  backgrounds: {
    default: "Dark300",
    values: [
      {
        name: "Dark300",
        value: colors.dark300,
      },
    ],
  },
};

export const Dark400 = Comp.bind({});
Dark400.args = {
  className:
    "cr-bgc-dark400 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Dark 400
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.dark400}
      </CRText>
    </CRView>
  ),
};
Dark400.parameters = {
  backgrounds: {
    default: "Dark400",
    values: [
      {
        name: "Dark400",
        value: colors.dark400,
      },
    ],
  },
};

import React from "react";
import { CRView } from "@mijo/components/CRView";
import { CRText } from "@mijo/components/CRText";
import { colors } from "@mijo/theme/colors";
import docs from "../../../../about-docs/foundations/colors/primaries/gray.md";

export default {
  title: "🧱 Foundations/🌈 Colors/Primaries/Gray",
  argTypes: {
    children: { control: { disable: true } },
    className: { control: { disable: true } },
  },
  parameters: {
    controls: { disabled: true },
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRView {...args} />;

export const Gray100 = Comp.bind({});
Gray100.args = {
  className: "cr-bgc-gray100 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant="subtitle2">
        Gray 100
      </CRText>
      <CRText align={"center"}>{colors.gray100}</CRText>
    </CRView>
  ),
};
Gray100.parameters = {
  backgrounds: {
    default: "Gray100",
    values: [
      {
        name: "Gray100",
        value: colors.gray100,
      },
    ],
  },
};

export const Gray200 = Comp.bind({});
Gray200.args = {
  className: "cr-bgc-gray200 cr-pdd-size25",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"}>
        Gray 200
      </CRText>
      <CRText align={"center"}>{colors.gray200}</CRText>
    </CRView>
  ),
};
Gray200.parameters = {
  backgrounds: {
    default: "Gray200",
    values: [
      {
        name: "Gray200",
        value: colors.gray200,
      },
    ],
  },
};

export const Gray300 = Comp.bind({});
Gray300.args = {
  className:
    "cr-bgc-gray300 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"}>
        Gray 300
      </CRText>
      <CRText align={"center"}>{colors.gray300}</CRText>
    </CRView>
  ),
};
Gray300.parameters = {
  backgrounds: {
    default: "Gray300",
    values: [
      {
        name: "Gray300",
        value: colors.gray300,
      },
    ],
  },
};

export const Gray400 = Comp.bind({});
Gray400.args = {
  className:
    "cr-bgc-gray400 cr-pdd-top-size30 cr-pdd-lft-size25 cr-pdd-rgt-size25 cr-pdd-btm-size30",
  children: (
    <CRView>
      <CRText align={"center"} variant={"subtitle2"} color={"white"}>
        Gray 400
      </CRText>
      <CRText align={"center"} color={"white"}>
        {colors.gray400}
      </CRText>
    </CRView>
  ),
};
Gray400.parameters = {
  backgrounds: {
    default: "Gray400",
    values: [
      {
        name: "Gray400",
        value: colors.gray400,
      },
    ],
  },
};

import React from 'react';
import { CRView } from '@mijo/components/CRView';
import { CRText } from '@mijo/components/CRText';
import { CRSpace } from '@mijo/components/CRSpace';
import { CRTextInput } from '@mijo/components/CRTextInput';
//Docs
import docs from '@mijo/components/CRTextInput/README.md';

export default {
  title: '🤖 Components/🕹️ Inputs/CRTextInput/Single Line',
  component: CRTextInput,
  argTypes: {
    iconOnPress: { action: 'clicked' },
  },
  parameters: {
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const SingleLineNoErrors = (args) => <CRTextInput {...args} />;
const SingleLineErrors = (args) => (
  <CRView>
    <CRText>Write something on the input to trigger the validation.</CRText>
    <CRSpace.Vertical variant={'medium'} />
    <CRTextInput {...args} />
  </CRView>
);
const SingleLineIcon = (args) => (
  <CRView>
    <CRText>This input has an button at the end represented by an icon.</CRText>
    <CRSpace.Vertical variant={'medium'} />
    <CRTextInput {...args} Iconvariant={'question'} />
  </CRView>
);

export const Default = SingleLineNoErrors.bind({});
Default.args = {
  placeholder: 'Placeholder',
  validation: true,
};

export const Error = SingleLineErrors.bind({});
Error.args = {
  placeholder: 'Placeholder',
  validation: false,
  errorMessage: 'Invalid input detected!',
};

export const With_icon_button = SingleLineIcon.bind({});
With_icon_button.args = {
  placeholder: 'Placeholder',
  iconVariant: 'search',
  validation: true,
};

import React from 'react';
import { CRButton } from '@mijo/components/CRButton';
//Docs
import docs from '@mijo/components/CRButton/README.md';

export default {
  title: '🤖 Components/🕹️ Inputs/CRButton',
  component: CRButton,
  argTypes: {
    onPress: { action: 'clicked' },
    textColor: { control: 'text' },
    color: { control: 'text' },
    disabled: { control: 'boolean' },
    addedStyle: { control: { disable: true } },
    textStyle: { control: { disable: true } },
  },
  parameters: {
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const MainComp = (args) => <CRButton {...args} />;
const GhostComp = (args) => <CRButton.Ghost {...args} />;
const IconComp = (args) => <CRButton.Icon {...args} />;

export const Default = MainComp.bind({});
Default.parameters = {
  docs: {
    source: {
      code: `<CRButton />`,
    },
  },
};

export const Ghost = GhostComp.bind({});
Ghost.parameters = {
  docs: {
    source: {
      code: `<CRButton.Ghost />`,
    },
  },
};

export const Icon = IconComp.bind({});
Icon.parameters = {
  docs: {
    source: {
      code: `<CRButton.Icon />`,
    },
  },
};

export const Disabled = MainComp.bind({});
Disabled.args = {
  disabled: true,
};

import React from "react";
import { CRImg } from "@mijo/components/CRImg";
import docs from "@mijo/components/CRImg/README.md";
import customPlaceholder from "@mijo/theme/assets/images/placeholder/placeholderUser.jpg";

export default {
  title: "🤖 Components/📸 Media/CRImg",
  component: CRImg,
  argTypes: {},
  parameters: {
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRImg {...args} />;

export const Default_Placeholder = Comp.bind({});
Default_Placeholder.args = {};
Default_Placeholder.parameters = {
  docs: {
    source: {
      code: `<CRImg />`,
    },
  },
};

export const Custom_Placeholder = Comp.bind({});
Custom_Placeholder.args = {
  placeholderImg: customPlaceholder,
};
Custom_Placeholder.parameters = {
  docs: {
    source: {
      code: `<CRImg placeholderImg={'/static/media/placeholderUser.46280382.jpg'} />`,
    },
  },
};

export const With_Image = Comp.bind({});
With_Image.args = {
  source:
    "https://i.pinimg.com/originals/da/e9/d5/dae9d5fc0800f12f5c720be598b6bea6.png",
};
With_Image.parameters = {
  docs: {
    source: {
      code: `<CRImg source={'https://i.pinimg.com/originals/da/e9/d5/dae9d5fc0800f12f5c720be598b6bea6.png'} />`,
    },
  },
};

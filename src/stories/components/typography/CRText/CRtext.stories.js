import React from 'react';
import { CRText } from '@mijo/components/CRText';
import docs from '@mijo/components/CRText/README.md';

export default {
  title: '🤖 Components/🖊️ Typography/CRText',
  argTypes: {
    limit: {
      control: {
        type: 'number',
      },
    },
    variant: {
      control: {
        type: 'select',
        options: [
          'title',
          'subtitle',
          'subtitle2',
          'subtitle3',
          'paragraph',
          'bold',
          'micro',
        ],
      },
    },
  },
  parameters: {
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRText {...args} />;

export const Title = Comp.bind({});
Title.args = {
  variant: 'title',
  children: 'This is a title.',
};
Title.parameters = {
  source: {
    code: `<CRText variant={'title'} />`,
  },
};

export const Subtitle = Comp.bind({});
Subtitle.args = {
  variant: 'subtitle',
  children: 'This is a subtitle.',
};
Subtitle.parameters = {
  source: {
    code: `<CRText variant={'subtitle'} />`,
  },
};

export const Subtitle2 = Comp.bind({});
Subtitle2.args = {
  variant: 'subtitle2',
  children: 'This is a smaller subtitle.',
};
Subtitle2.parameters = {
  source: {
    code: `<CRText variant={'subtitle2'} />`,
  },
};

export const Subtitle3 = Comp.bind({});
Subtitle3.args = {
  variant: 'subtitle3',
  children: 'This is the smallest subtitle.',
};
Subtitle3.parameters = {
  source: {
    code: `<CRText variant={'subtitle3'} />`,
  },
};

export const Paragraph = Comp.bind({});
Paragraph.args = {
  variant: 'paragraph',
  children: 'This is a paragraph.',
};
Paragraph.parameters = {
  source: {
    code: `<CRText variant={'paragraph'} />`,
  },
};

export const Caption = Comp.bind({});
Caption.args = {
  variant: 'caption',
  children: 'This is a caption.',
};
Caption.parameters = {
  source: {
    code: `<CRText variant={'caption'} />`,
  },
};

export const Micro = Comp.bind({});
Micro.args = {
  variant: 'micro',
  children: 'This is some micro text.',
};
Micro.parameters = {
  docs: {
    source: {
      code: `<CRText variant={'micro'} />`,
    },
  },
};

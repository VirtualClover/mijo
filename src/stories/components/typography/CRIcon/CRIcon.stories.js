import React from 'react';
import { CRIcon } from '@mijo/components/CRIcon';
import { CRText } from '@mijo/components/CRText';
import docs from '@mijo/components/CRText/README.md';

export default {
  title: '🤖 Components/🖊️ Typography/CRIcon',
  argTypes: {},
  parameters: {
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRIcon {...args} />;
const CompRow = (args) => (
  <CRText>
    <CRIcon variant="basket" />
    <CRIcon variant="calendar" />
    <CRIcon variant="categories" />
    <CRIcon variant="filter" />
    <CRIcon variant="orders" />
  </CRText>
);

export const Default = Comp.bind({});
Default.args = {
  size: 36,
  variant: 'basket',
  className: 'cr__text--title',
};

export const Various_icons = CompRow.bind({});

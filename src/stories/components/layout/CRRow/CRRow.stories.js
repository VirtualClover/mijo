/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from "react";
import { CRRow } from "@mijo/components/CRRow";
import { CRSpace } from "@mijo/components/CRSpace";
import { CRButton } from "@mijo/components/CRButton";
//Docs
import docs from "@mijo/components/CRRow/README.md";

export default {
  title: "🤖 Components/🏗️ Layout/CRRow",
  component: CRRow,
  argTypes: {
    children: { control: { disable: true } },
  },
  parameters: {
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => (
  <CRRow {...args}>
    <CRButton />
    <CRSpace.Horizontal />
    <CRButton />
  </CRRow>
);

export const Default = Comp.bind({});
Default.args = {};
Default.parameters = {
  docs: {
    source: {
      code: `<CRRow />`,
    },
  },
};

export const Debug_mode = Comp.bind({});
Debug_mode.args = {
  debug: true,
};
Debug_mode.parameters = {
  docs: {
    source: {
      code: `<CRRow debug />`,
    },
  },
};

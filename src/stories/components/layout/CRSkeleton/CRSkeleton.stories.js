import React from 'react';
import { CRSkeleton } from '@mijo/components/CRSkeleton';
import docs from '@mijo/components/CRSkeleton/README.md';
import { CRView } from '@mijo/components/CRView';

export default {
  title: '🤖 Components/🏗️ Layout/CRSkeleton',
  component: CRSkeleton,
  parameters: {
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => (
  <CRView className={'cr-wdt-size100'}>
    <CRSkeleton {...args} style={{ width: 100 }} />
  </CRView>
);
const CompText = (args) => (
  <CRView className={'cr-wdt-size100'}>
    <CRSkeleton.Text {...args} style={{ width: 100 }} />
  </CRView>
);

const CompImg = (args) => (
  <CRView className={'cr-wdt-size100'}>
    <CRSkeleton.Image {...args} style={{ width: 100 }} />
  </CRView>
);

export const Block = Comp.bind({});
Block.args = {};
Block.parameters = {
  docs: {
    source: {
      code: `<CRSkeleton />`,
    },
  },
};
export const Text = CompText.bind({});
Text.args = {
  textVariant: 'subtitle3',
};
Text.argTypes = {
  textVariant: {
    control: {
      type: 'select',
      options: [
        'title',
        'subtitle',
        'subtitle2',
        'subtitle3',
        'paragraph',
        'bold',
        'caption',
        'micro',
      ],
    },
  },
};
Text.parameters = {
  docs: {
    source: {
      code: `<CRSkeleton variant={'text'}/>`,
    },
  },
};

export const Image = CompImg.bind({});
Image.parameters = {
  docs: {
    source: {
      code: `<CRSkeleton variant={'image'}/>`,
    },
  },
};

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from "react";
import { CRSection } from "@mijo/components/CRSection";
import { CRText } from "@mijo/components/CRText";
//Docs
import docs from "@mijo/components/CRSection/README.md";

export default {
  title: "🤖 Components/🏗️ Layout/CRSection",
  component: CRSection,
  argTypes: {
    children: { control: { disable: true } },
    debugTooltipClass: { control: { disable: true } },
  },
  parameters: {
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRSection {...args} />;

export const Debug_mode = Comp.bind({});
Debug_mode.args = {
  children: <CRText>This is a Section in debug mode</CRText>,
  debug: true,
};
Debug_mode.parameters = {
  docs: {
    source: {
      code: `<CRSection debug ><CRText>This is a View in debug mode</CRText></CRSection>`,
    },
  },
};

export const With_title = Comp.bind({});
With_title.args = {
  children: <CRText>This is some text within a section</CRText>,
  title: "Section 1",
  debug: false,
};
With_title.parameters = {
  docs: {
    source: {
      code: `<CRSection title={'Section 1'}><CRText>This is some text within a section</CRText>`,
    },
  },
};

export const With_title_and_description = Comp.bind({});
With_title_and_description.args = {
  children: <CRText>This is some text within a section</CRText>,
  title: "Section 1",
  desc: "This is a description for said section",
  debug: false,
};
With_title_and_description.parameters = {
  docs: {
    source: {
      code: `<CRSection title={'Section 1'}><CRText>This is some text within a section</CRText>`,
    },
  },
};

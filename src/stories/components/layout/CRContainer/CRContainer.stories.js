/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from "react";
import { CRContainer } from "@mijo/components/CRContainer";
import { CRText } from "@mijo/components/CRText";
import { CRSection } from "@mijo/components/CRSection";
import { colors } from "@mijo/theme/colors";
//Docs
import docs from "@mijo/components/CRContainer/README.md";

export default {
  title: "🤖 Components/🏗️ Layout/CRContainer",
  component: CRContainer,
  argTypes: {
    children: { control: { disable: true } },
  },
  parameters: {
    backgrounds: {
      default: "Gray100",
      values: [
        {
          name: "Gray100",
          value: colors.gray100,
        },
      ],
    },
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => (
  <CRContainer {...args}>
    <CRSection>
      <CRText variant={"title"}>This is some text inside a container</CRText>
    </CRSection>
  </CRContainer>
);

export const Default = Comp.bind({});
Default.args = {};
Default.parameters = {
  docs: {
    source: {
      code: `<CRContainer />`,
    },
  },
};

export const Debug_mode = Comp.bind({});
Debug_mode.args = {
  debug: true,
};
Debug_mode.parameters = {
  docs: {
    source: {
      code: `<CRContainer debug />`,
    },
  },
};

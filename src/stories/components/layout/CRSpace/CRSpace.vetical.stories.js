/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from "react";
import { CRSpace } from "@mijo/components/CRSpace";
//Docs
import docs from "@mijo/components/CRSpace/README.md";

export default {
  title: "🤖 Components/🏗️ Layout/CRSpace/Vertical",
  component: CRSpace,
  argTypes: {
    variant: {
      control: {
        type: "select",
        options: ["xSmall", "small", "medium", "large", "xLarge", "xxLarge"],
      },
    },
  },
  parameters: {
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const CompVar1 = (args) => <CRSpace.Vertical {...args} />;

export const Debug_mode = CompVar1.bind({});
Debug_mode.args = {
  debug: true,
  variant: "medium",
};
Debug_mode.parameters = {
  docs: {
    source: {
      code: `<CRSpace.Vertical debug />`,
    },
  },
};

export const Small = CompVar1.bind({});
Small.args = {
  debug: true,
  variant: "small",
};
Small.parameters = {
  docs: {
    source: {
      code: `<CRSpace.Vertical debug  variant={'small'} />`,
    },
  },
};

export const Medium = CompVar1.bind({});
Medium.args = {
  debug: true,
  variant: "medium",
};
Medium.parameters = {
  docs: {
    source: {
      code: `<CRSpace.Vertical debug  variant={'medium'} />`,
    },
  },
};

export const Large = CompVar1.bind({});
Large.args = {
  debug: true,
  variant: "large",
};
Large.parameters = {
  docs: {
    source: {
      code: `<CRSpace.Vertical debug  variant={'large'} />`,
    },
  },
};

export const XLarge = CompVar1.bind({});
XLarge.args = {
  debug: true,
  variant: "xLarge",
};
XLarge.parameters = {
  docs: {
    source: {
      code: `<CRSpace.Vertical debug  variant={'xLarge'} />`,
    },
  },
};

export const XXLarge = CompVar1.bind({});
XXLarge.args = {
  debug: true,
  variant: "xxLarge",
};
XXLarge.parameters = {
  docs: {
    source: {
      code: `<CRSpace.Vertical debug  variant={'xxLarge'} />`,
    },
  },
};

export const With_number = CompVar1.bind({});
With_number.args = {
  debug: true,
  variantNumber: 35,
};
With_number.parameters = {
  docs: {
    source: {
      code: `<CRSpace.Vertical debug  variantNumber={35} />`,
    },
  },
};

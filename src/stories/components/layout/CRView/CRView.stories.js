/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from "react";
import { CRView } from "@mijo/components/CRView";
import { CRText } from "@mijo/components/CRText";
//Docs
import docs from "@mijo/components/CRView/README.md";

export default {
  title: "🤖 Components/🏗️ Layout/CRView",
  component: CRView,
  argTypes: {
    children: { control: { disable: true } },
    debugTooltipClass: { control: { disable: true } },
  },
  parameters: {
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRView {...args} />;

export const Debug_mode = Comp.bind({});
Debug_mode.args = {
  children: <CRText>This is a View in debug mode</CRText>,
  debug: true,
};
Debug_mode.parameters = {
  docs: {
    source: {
      code: `<CRView debug ><CRText>This is a View in debug mode</CRText></CRView>`,
    },
  },
};

export const Custom_tooltip_name = Comp.bind({});
Custom_tooltip_name.args = {
  children: <CRText>This is a View with a custom debug tooltip name</CRText>,
  debug: true,
  debugTooltipName: "Custom Name",
};
Custom_tooltip_name.parameters = {
  docs: {
    source: {
      code: `<CRView debugTooltipName={'Custom Name'} ><CRText>This is a View with a custom debug tooltip name</CRText></CRView>`,
    },
  },
};

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
import { CRError } from '@mijo/components/CRError';
//Docs
import docs from '@mijo/components/CRError/README.md';

export default {
  title: '🤖 Components/💻 Development/CRError',
  component: CRError,
  parameters: {
    docs: {
      description: {
        component: docs,
      },
    },
  },
};

const Comp = (args) => <CRError {...args} />;

export const Default = Comp.bind({});
Default.args = {
  component: 'CRExampleComponent',
  error: 'Example error log',
};

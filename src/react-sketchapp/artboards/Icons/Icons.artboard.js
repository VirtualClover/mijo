/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import * as React from 'react';
//Mijo DS
import { SCRArtboard } from '../../../core/sketch/SCRArtboard';
//Styles
import { styles } from './Icons.styles';
import { CRRow } from '../../../core/components/CRRow';
import { CRIcon } from '../../../core/components/CRIcon';
import { CRSpace } from '../../../core/components/CRSpace';
import { CRView } from '../../../core/components/CRView';
import { CRText } from '../../../core/components/CRText';

//DATA
import DATA from '../../../core/theme/assets/fonts/canasta_sans/selection.json';

export const Icons = () => {
  const iconData = [...DATA.icons];
  //Splice Array in columns of 3
  var columns = [];
  var size = 5;

  while (iconData.length > 0) columns.push(iconData.splice(0, size));
  return (
    <SCRArtboard
      doc
      title={'Icons'}
      desc={
        'This is our library of icons, if you have trouble seen them you might wanna install our icon font canasta-sans.'
      }
      style={styles.artboard}
    >
      {columns.map(function (column, i) {
        return (
          <CRRow key={i}>
            {column.map(function (data, i) {
              return (
                <CRView key={i}>
                  <CRSpace.Horizontal
                    variantNumber={110}
                    style={{ height: 20 }}
                  />
                  <CRIcon variant={data.properties.name} key={i} />
                  <CRSpace.Vertical variant={'xSmall'} />
                  <CRText align={'center'} variant={'caption'}>
                    {data.properties.name}
                  </CRText>
                  <CRSpace.Vertical variant={'xxLarge'} />
                </CRView>
              );
            })}
          </CRRow>
        );
      })}
    </SCRArtboard>
  );
};

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import { StyleSheet } from 'react-sketchapp';

export const styles = StyleSheet.create({
  artboard: {
    width: 550,
  },
});

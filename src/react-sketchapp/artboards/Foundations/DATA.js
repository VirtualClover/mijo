import { colors } from "../../../core/theme/colors";
export const DATA = [
  {
    group: "Main",
    desc: `Canasta's main color.`,
    shades: [
      {
        color: colors.main100,
        colorName: "Main100",
      },
      {
        color: colors.main200,
        colorName: "Main200",
      },
      {
        color: colors.main300,
        colorName: "Main300",
      },
      {
        color: colors.main400,
        colorName: "Main400",
      },
    ],
  },
  {
    group: "Deep",
    desc: `As of MIJO 1.1.0 This is the complementary color of our main one.`,
    shades: [
      {
        color: colors.deep100,
        colorName: "Deep100",
      },
      {
        color: colors.deep200,
        colorName: "Deep200",
      },
      {
        color: colors.deep300,
        colorName: "Deep300",
      },
      {
        color: colors.deep400,
        colorName: "Deep400",
      },
    ],
  },
  {
    group: "Dark",
    desc: `This is the color that we use for text`,
    shades: [
      {
        color: colors.dark100,
        colorName: "Dark100",
      },
      {
        color: colors.dark200,
        colorName: "Dark200",
      },
      {
        color: colors.dark300,
        colorName: "Dark300",
      },
      {
        color: colors.dark400,
        colorName: "Dark400",
      },
    ],
  },
  {
    group: "Gray",
    desc: `These are the gray shades of our site.`,
    shades: [
      {
        color: colors.gray100,
        colorName: "Gray100",
      },
      {
        color: colors.gray200,
        colorName: "Gray200",
      },
      {
        color: colors.gray300,
        colorName: "Gray300",
      },
      {
        color: colors.gray400,
        colorName: "Gray400",
      },
    ],
  },
  {
    group: "Yellow",
    desc: `A true sun beam.`,
    shades: [
      {
        color: colors.yellow100,
        colorName: "Yellow100",
      },
      {
        color: colors.yellow200,
        colorName: "Yellow200",
      },
      {
        color: colors.yellow300,
        colorName: "Yellow300",
      },
      {
        color: colors.yellow400,
        colorName: "Yellow400",
      },
    ],
  },
  {
    group: "Turquiose",
    desc: ``,
    shades: [
      {
        color: colors.turquiose100,
        colorName: "Turquiose100",
      },
      {
        color: colors.turquiose200,
        colorName: "Turquiose200",
      },
      {
        color: colors.turquiose300,
        colorName: "Turquiose300",
      },
      {
        color: colors.turquiose400,
        colorName: "Turquiose400",
      },
    ],
  },
  {
    group: "Red",
    desc: ``,
    shades: [
      {
        color: colors.red100,
        colorName: "Red100",
      },
      {
        color: colors.red200,
        colorName: "Red200",
      },
      {
        color: colors.red300,
        colorName: "Red300",
      },
      {
        color: colors.red400,
        colorName: "Red400",
      },
    ],
  },
  {
    group: "Orange",
    desc: ``,
    shades: [
      {
        color: colors.orange100,
        colorName: "Orange100",
      },
      {
        color: colors.orange200,
        colorName: "Orange200",
      },
      {
        color: colors.orange300,
        colorName: "Orange300",
      },
      {
        color: colors.orange400,
        colorName: "Orange400",
      },
    ],
  },
  {
    group: "Peach",
    desc: ``,
    shades: [
      {
        color: colors.peach100,
        colorName: "Peach100",
      },
      {
        color: colors.peach200,
        colorName: "Peach200",
      },
      {
        color: colors.peach300,
        colorName: "Peach300",
      },
      {
        color: colors.peach400,
        colorName: "Peach400",
      },
    ],
  },
  {
    group: "Brown",
    desc: ``,
    shades: [
      {
        color: colors.brown100,
        colorName: "Brown100",
      },
      {
        color: colors.brown200,
        colorName: "Brown200",
      },
      {
        color: colors.brown300,
        colorName: "Brown300",
      },
      {
        color: colors.brown400,
        colorName: "Brown400",
      },
    ],
  },
  {
    group: "Lime",
    desc: ``,
    shades: [
      {
        color: colors.lime100,
        colorName: "Lime100",
      },
      {
        color: colors.lime200,
        colorName: "Lime200",
      },
      {
        color: colors.lime300,
        colorName: "Lime300",
      },
      {
        color: colors.lime400,
        colorName: "Lime400",
      },
    ],
  },
  {
    group: "Green",
    desc: ``,
    shades: [
      {
        color: colors.green100,
        colorName: "Green100",
      },
      {
        color: colors.green200,
        colorName: "Green200",
      },
      {
        color: colors.green300,
        colorName: "Green300",
      },
      {
        color: colors.green400,
        colorName: "Green400",
      },
    ],
  },
  {
    group: "Aqua",
    desc: ``,
    shades: [
      {
        color: colors.aqua100,
        colorName: "Aqua100",
      },
      {
        color: colors.aqua200,
        colorName: "Aqua200",
      },
      {
        color: colors.aqua300,
        colorName: "Aqua300",
      },
      {
        color: colors.aqua400,
        colorName: "Aqua400",
      },
    ],
  },
  {
    group: "Blue",
    desc: ``,
    shades: [
      {
        color: colors.blue100,
        colorName: "Blue100",
      },
      {
        color: colors.blue200,
        colorName: "Blue200",
      },
      {
        color: colors.blue300,
        colorName: "Blue300",
      },
      {
        color: colors.blue400,
        colorName: "Blue400",
      },
    ],
  },
  {
    group: "Violet",
    desc: ``,
    shades: [
      {
        color: colors.violet100,
        colorName: "Violet100",
      },
      {
        color: colors.violet200,
        colorName: "Violet200",
      },
      {
        color: colors.violet300,
        colorName: "Violet300",
      },
      {
        color: colors.violet400,
        colorName: "Violet400",
      },
    ],
  },
  {
    group: "Purple",
    desc: ``,
    shades: [
      {
        color: colors.purple100,
        colorName: "Purple100",
      },
      {
        color: colors.purple200,
        colorName: "Purple200",
      },
      {
        color: colors.purple300,
        colorName: "Purple300",
      },
      {
        color: colors.purple400,
        colorName: "Purple400",
      },
    ],
  },
];

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import * as React from 'react';
//Mijo DS
import { SCRArtboard } from '../../../core/sketch/SCRArtboard';
import { CRView } from '../../../core/components/CRView';
//Styles
import { styles } from './Foundations.styles';
//Artboard Sections
import * as Sections from './sections';
import { CRRow } from '../../../core/components/CRRow';
import { CRSpace } from '../../../core/components/CRSpace';

export const Foundations = () => {
  return (
    <SCRArtboard
      doc
      title={'Foundations'}
      desc={'These are the pillars of our design system'}
      style={styles.artboard}
    >
      <CRRow alignItems={'flex-start'}>
        <CRSpace.Horizontal variant={'large'} />
        <CRView style={styles.colorSection}>
          <Sections.Colors />
        </CRView>
        <CRSpace.Horizontal variant={'xLarge'} />
        <CRView>
          <Sections.Typography />
          <CRSpace.Vertical variant={'large'} />
          <Sections.Brand />
        </CRView>
        <CRSpace.Horizontal variant={'large'} />
      </CRRow>
    </SCRArtboard>
  );
};

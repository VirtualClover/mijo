/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import * as React from "react";
//Mijo DS
import { CRSection } from "../../../../../core/components/CRSection";
import { SCRArtboard } from "../../../../../core/sketch/SCRArtboard";
import { CRText } from "../../../../../core/components/CRText";
import { CRView } from "../../../../../core/components/CRView";
import { CRSpace } from "../../../../../core/components/CRSpace";
import { styles } from "./TypographySection.style";
import { CRRow } from "../../../../../core/components/CRRow";

const PHRASE = "The quick brown fox jumps over the lazy dog.";

const ShowCase = ({ variant, specs, children }) => {
  return (
    <CRView>
      <CRText variant={variant}>{children}</CRText>
      <CRRow>
        <CRText ariant={"caption"} color={"dark200"}>
          {variant}
        </CRText>
        <CRText variant={"caption"} color={"dark200"}>
          {` ${specs}`}
        </CRText>
      </CRRow>
      <CRSpace.Vertical variant={"small"} />
    </CRView>
  );
};

//Artboard
export const Typography = () => {
  return (
    <CRSection
      name={"typography"}
      title={"Typography"}
      desc={"These are the basics of our typography beahivours."}
    >
      <ShowCase variant={"title"} specs={"38/40"}>
        {PHRASE}
      </ShowCase>
      <ShowCase variant={"subtitle"} specs={"26/28"}>
        {PHRASE}
      </ShowCase>
      <ShowCase variant={"subtitle2"} specs={"22/24"}>
        {PHRASE}
      </ShowCase>
      <ShowCase variant={"subtitle3"} specs={"18/20"}>
        {PHRASE}
      </ShowCase>
      <ShowCase variant={"paragraph"} specs={"16/18"}>
        {PHRASE}
      </ShowCase>
      <ShowCase variant={"caption"} specs={"13/15"}>
        {PHRASE}
      </ShowCase>
      <ShowCase variant={"micro"} specs={"10/12"}>
        {PHRASE}
      </ShowCase>
    </CRSection>
  );
};

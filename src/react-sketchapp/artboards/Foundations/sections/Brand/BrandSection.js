/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import * as React from "react";
//Mijo DS
import { CRView } from "../../../../../core/components/CRView";
import { CRText } from "../../../../../core/components/CRText";
import { CRSection } from "../../../../../core/components/CRSection";
import { CRRow } from "../../../../../core/components/CRRow";
import { CRSpace } from "../../../../../core/components/CRSpace";
import { styles } from "./BrandSection.style";
//Symbols
import { Symbols } from "../../../../symbols";

const BrandDisplay = (props) => {
  return (
    <CRView style={styles.brandDisplay}>
      {props.children}
      <CRSpace.Vertical variantNumber={5} />
      <CRText variant={"caption"}>{props.variant}</CRText>
    </CRView>
  );
};

//Artboard
export const Brand = () => {
  return (
    <CRSection
      name={"Brand"}
      title={"Brand"}
      desc={"These are our brand assets."}
    >
      <CRRow>
        <BrandDisplay variant={"Main logo"}>
          <Symbols.CRLogoMain />
        </BrandDisplay>
        <CRSpace.Horizontal variant={"small"} />
        <BrandDisplay variant={"Negative logo"}>
          <CRView style={styles.darkWrap}>
            <Symbols.CRLogoNegative />
          </CRView>
        </BrandDisplay>
      </CRRow>
      <CRSpace.Vertical variant={"medium"} />
      <CRRow>
        <BrandDisplay variant={"Isotype"}>
          <CRView style={styles.isoDarkWrap}>
            <Symbols.CRLogoIsotype />
          </CRView>
        </BrandDisplay>
        <CRSpace.Horizontal variant={"small"} />
        <BrandDisplay variant={"Isotype Negative"}>
          <CRView style={styles.isoDarkWrap}>
            <Symbols.CRLogoIsotypeNegative />
          </CRView>
        </BrandDisplay>
        <CRSpace.Horizontal variant={"small"} />
        <BrandDisplay variant={"Isotype White"}>
          <CRView style={styles.isoDarkWrap}>
            <Symbols.CRLogoIsotypeWhite />
          </CRView>
        </BrandDisplay>
        <CRSpace.Horizontal variant={"small"} />
        <BrandDisplay variant={"Isotype Pink"}>
          <CRView style={styles.isoDarkWrap}>
            <Symbols.CRLogoIsotypePink />
          </CRView>
        </BrandDisplay>
      </CRRow>
    </CRSection>
  );
};

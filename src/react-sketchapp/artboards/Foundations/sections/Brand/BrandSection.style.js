/*

This file was created by Canasta Rosa 
under the MIT license.
 
*/

import { StyleSheet } from 'react-sketchapp';
import { colors } from '../../../../../core/theme/colors';

export const styles = StyleSheet.create({
  brandDisplay: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  darkWrap: {
    backgroundColor: colors.deep300,
    borderRadius: 3,
  },
  isoDarkWrap: {
    backgroundColor: colors.deep300,
    borderRadius: 3,
    width: 51,
  },
});

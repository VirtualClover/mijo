/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import * as React from "react";
//Mijo DS
import { CRSection } from "../../../../../core/components/CRSection";
import { SCRSwatch } from "../../../../../core/sketch/SCRSwatch";
import { CRText } from "../../../../../core/components/CRText";
import { CRView } from "../../../../../core/components/CRView";
import { CRRow } from "../../../../../core/components/CRRow";
//Styles
import { styles } from "./ColorsSection.style";
//Artboard DATA
import { DATA } from "../../DATA";
import { CRSpace } from "../../../../../core/components/CRSpace";

export const Colors = ({}) => {
  //Splice Array in columns of 3
  var columns = [],
    size = 3;

  while (DATA.length > 0) columns.push(DATA.splice(0, size));

  return (
    <CRSection
      name={"Colors"}
      title={"Colors"}
      desc={`These are the colors that form our pretty site, every shade has a scale from 100 to 400; 100 been the lightest and 400 been the darkest and been 300 the default shade, please use it wisely.`}
    >
      {columns.map(function (column, i) {
        return (
          <CRRow key={i} name={"Color Row"} justifyContent={"space-between"}>
            {column.map(function (data, i) {
              return (
                <CRView key={i} name={data.group}>
                  <CRView style={styles.groupDesc} name={"Color description"}>
                    <CRText variant={"subtitle2"} color={"dark200"}>
                      {data.group}
                    </CRText>
                    <CRSpace.Vertical variant={"xSmall"} />
                  </CRView>
                  <CRView style={styles.swatchWrapper} name={"Swatch wrapper"}>
                    {data.shades.map(function (shade, i) {
                      return (
                        <SCRSwatch
                          key={i}
                          colorName={shade.colorName}
                          color={shade.color}
                          colorDesc={shade.color}
                        />
                      );
                    })}
                  </CRView>
                  <CRSpace.Vertical size={"large"} />
                </CRView>
              );
            })}
          </CRRow>
        );
      })}
    </CRSection>
  );
};

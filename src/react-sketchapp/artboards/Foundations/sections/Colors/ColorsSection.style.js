/*

This file was created by Canasta Rosa 
under the MIT license.
 
*/

import { StyleSheet } from "react-sketchapp";

export const styles = StyleSheet.create({
  row: {
    justifyContent: "space-between",
  },
  groupDesc: {
    maxWidth: 480,
  },
  swatchWrapper: {
    flexDirection: "row",
  },
});

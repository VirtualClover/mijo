/*

This file was created by Canasta Rosa 
under the MIT license.
 
*/

import { StyleSheet } from "react-primitives";

export const styles = StyleSheet.create({
  artboard: {
    alignSelf: "stretch",
  },
  colorSection: {
    width: 1540,
  },
});

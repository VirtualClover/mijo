/*

This file was created by Canasta Rosa 
under the MIT license.
 
*/

import { StyleSheet } from 'react-primitives';
import { colors } from '../../../core/theme/colors';

export const styles = StyleSheet.create({
  artboard: {
    width: 750,
  },
  darkWrap: {
    backgroundColor: colors.deep300,
    padding: 5,
    borderRadius: 3,
    width: 160,
  },
  reel: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 700,
  },
});

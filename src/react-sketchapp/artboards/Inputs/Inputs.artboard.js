/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import * as React from 'react';
//Mijo DS
import { SCRArtboard } from '../../../core/sketch/SCRArtboard';
import { CRSection } from '../../../core/components/CRSection';
import { CRText } from '../../../core/sketch/CRText';
import { CRView } from '../../../core/sketch/CRView';
import { Symbols } from '../../symbols';
import { styles } from './Inputs.style';
import { CRButton } from '../../../core/components/CRButton';
//Artboard
export const Inputs = () => {
  return (
    <SCRArtboard
      doc
      title={'Inputs'}
      desc={
        ' Inputs are the signals or data received by the system and outputs are the signals or data sent from it. The term can also be used as part of an action.'
      }
      style={styles.artboard}
    >
      <CRSection>
        <CRButton>Hola!</CRButton>
        <CRText variant="title">Buttons</CRText>
        <CRView style={styles.reel}>
          <Symbols.CRBDefault />
          <Symbols.CRBPinkGhost />
          <CRView style={styles.darkWrap}>
            <Symbols.CRBWhite />
          </CRView>
          <CRView style={styles.darkWrap}>
            <Symbols.CRBWhiteGhost />
          </CRView>
          <Symbols.CRBIcon />
        </CRView>
      </CRSection>
    </SCRArtboard>
  );
};

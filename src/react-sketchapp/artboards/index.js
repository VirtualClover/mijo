/*

This file was created by Canasta Rosa 
under the MIT license.

*/

export { Foundations } from './Foundations/Foundations.artboard';
export { Inputs } from './Inputs/Inputs.artboard';
export { Icons } from './Icons/Icons.artboard';

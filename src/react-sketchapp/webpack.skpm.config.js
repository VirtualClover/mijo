const path = require("path");

module.exports = (config) => {
  config.resolve = {
    ...config.resolve,
    alias: {
      ...config.resolve.alias,
      "react-native$": "react-primitives",
      "^(.+?)\\.scss$": "$1.js",
      "^(.+?)\\.style.scss$": "$1.style.js",
      //'#MDSCore': path.resolve(__dirname, 'src/core/sketch'),
    },
  };
};

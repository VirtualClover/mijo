import * as React from 'react';
import { render, Document, Page } from 'react-sketchapp';
import * as Board from '../artboards';

const App = ({}) => (
  <Document>
    <Page name={'Foundations'}>
      <Board.Foundations />
    </Page>
    <Page name={'Inputs'}>
      <Board.Inputs />
    </Page>
    <Page name={'Icons'}>
      <Board.Icons />
    </Page>
  </Document>
);

export default () => {
  render(<App />);
};

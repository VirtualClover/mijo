/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
import { makeSymbol } from 'react-sketchapp';
//MIJO DS
import { CRButton } from '../../core/components/CRButton';
import { CRLogo } from '../../core/components/CRLogo';

//Declare symbols as constants
const CRBDefault = () => <CRButton>Default</CRButton>;
const CRBPinkGhost = () => <CRButton.Ghost>Pink Ghost</CRButton.Ghost>;
const CRBWhite = () => (
  <CRButton color="white" textColor="main300">
    White
  </CRButton>
);
const CRBWhiteGhost = () => (
  <CRButton.Ghost color="white" textColor="white">
    White Ghost
  </CRButton.Ghost>
);
const CRBIcon = () => <CRButton.Icon iconVariant="cross" />;
const CRLogoMain = () => <CRLogo.Main />;
const CRLogoNegative = () => <CRLogo.Negative />;
const CRLogoIsotype = () => <CRLogo.Isotype />;
const CRLogoIsotypeNegative = () => <CRLogo.IsotypeNegative />;
const CRLogoIsotypeWhite = () => <CRLogo.IsotypeWhite />;
const CRLogoIsotypePink = () => <CRLogo.IsotypePink />;

//Create symbols from those constants
export const Symbols = {
  //Logo
  CRLogoMain: makeSymbol(CRLogoMain, 'CRLogo/Main'),
  CRLogoNegative: makeSymbol(CRLogoNegative, 'CRLogo/Negative'),
  CRLogoIsotype: makeSymbol(CRLogoIsotype, 'CRLogo/Isotype'),
  CRLogoIsotypeNegative: makeSymbol(
    CRLogoIsotypeNegative,
    'CRLogo/Isotype Negative'
  ),
  CRLogoIsotypeWhite: makeSymbol(CRLogoIsotypeWhite, 'CRLogo/Isotype White'),
  CRLogoIsotypePink: makeSymbol(CRLogoIsotypePink, 'CRLogo/Isotype Pink'),
  //Button
  CRBPinkGhost: makeSymbol(CRBPinkGhost, 'CRButton/Pink Ghost'),
  CRBDefault: makeSymbol(CRBDefault, 'CRButton/Default'),
  CRBWhite: makeSymbol(CRBWhite, 'CRButton/White'),
  CRBWhiteGhost: makeSymbol(CRBWhiteGhost, 'CRButton/White Ghost'),
  CRBIcon: makeSymbol(CRBIcon, 'CRButton/Icon'),
};

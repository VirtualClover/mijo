# Pillar Libraries

These project has been built out of three pillar libraries.

- <a href="https://github.com/airbnb/react-sketchapp" target="_blank"><strong>React-SketchApp by airbnb</strong></a>  
  For rendering React components into sketch and creating the MIJO sketch plugin.
- <a href="https://github.com/lelandrichardson/react-primitives" target="_blank"><strong>React primitives by lelandrichardson</strong></a>  
  For rendering React Native components to web, sketch and React Native within a single file.
- <a href="https://github.com/storybookjs/storybook" target="_blank"><strong>Storybook by the Storybook Community and 2 add-ons</strong></a>

  - '@storybook/addon-docs' (essential)
  - '@storybook/theming' (non-essential)

  For documentation and visual testing.

## Doomsday scenarios

A small list of possible scenarios where the DS can be affected by the deprecation or deletion of the libraries above, and quick solutions for them.

### • React-Sketchapp is deleted.

Coded components no longer be converted to sketch symbols.

#### Solution

No biggie, make the sketch components by hand lazy. You can look for libraries that can compile javascript to cocoascript, babel would be a good fisrt guess.

### • React primitives is deleted.

React native components can no longer be displayed on storybook and can't be rendered on the other projects and the components for react native will be temporarly borken.

#### Solution

First we need to replace all the **View** and **Text** imports in the React native version of the DS from **'react-primitives'** to **'react-native'**.
Second, we can't no longer render react native components directly in Sketch, you will need to create versions of the same components for react native with the fixed import mentioned before and a Sketch version with the **View** and **Text** components imported from **'react-sketchapp'**.

### • Storybook is deleted or any of the addons.

The documentation page can no longer be build.

#### Solution

Make a new docs page, there are many docs page libraries for react. Or just keept the README files for a quick fix.

### • ALL OF THE ABOVE

The documentation page can no longer be build, and the components for react native can no longer be linked to sketch, and the components for react native will be temporarly borken.

#### Solution

First we need to replace all the **View** and **Text** imports in the React native version of the DS from **'react-primitives'** to **'react-native'**.

Next we need to import our sketch libraries to invision using craft and find a new viable way to maintain the ds.

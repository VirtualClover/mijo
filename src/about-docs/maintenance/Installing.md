# MIJO local enviroment install

## Requierements

- A gitlab account
- Install NodeJS
- Git

## Installing the project

1. Git clone the project  
   `git clone git@gitlab.com:VirtualClover/mijo.git`

2. Install the dependecies  
   `npm run quick-start`

3. Run the project  
    You have various options from here on now.

   - If you want to start storybook  
     `npm run storybook`

   - If you want to test the sketch plugin  
     Create a new sketch file and then the in the terminal  
     `npm run render`

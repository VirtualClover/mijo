# Wireframing & prototyping

The MIJO framework uses Sketch as its main tool for prototyping and divides the wirframing process in 4 steps, according to the fidelity.

## 1. Low-fi wireframing

At the start of every project we usually do a planning meeting, here developers and designers converge to find the tasks that need to be done. Generally we draw the low wireframing in the form of blocks; from these we can see what are the requierements we need to have in terms of frontend, backend and design here's an example of what they look like.

## 2. Mid-fi wireframing

Here we adjust the blocks to more refined elements, as designers, we need to keep in touch with the developers to understand what can and can't be done in term of design, we also can start scoping more specifically the components that they will need to create and the specs they need to have.

> We do not use colors in mid-fi wireframes, we keep it simple, shapes and composition.

<img src="docs-images/wireframe-mid.png"/>

From this point on we need to adapt our designs to different displays wether it's web or app.

Remember designing from the narrowest to the widest display(mobile first).

### Web

- Desktop
- Tablet
- Mobile

### App

- Iphone SE
- Iphone 8
- Iphone X
- Nexus

## 3. High-fi wireframing

From this wireframe we are going to create the prototype so we need to refine every little detail from images, to colors, to copies.

<img src="docs-images/wireframe-high.png"/>

## Some tips

- Use the InVision API plugin for populating designs, check the Craft's plugin documentation for more info.
- Always ask for feedback from the tech team and/or other teams at Canasta, at the end of the day we are the customers, every person has a different skillset and priorities meaning that they have the ability to pinpoint flaws in our design that we never thought of.

## 4. Prototype

We uses prototypes to test our ideas with users. It's the final step, the end coded product needs to be as close as possible as this prototype.

The MIJO framework uses the InVision platform to create it, you can check an example <a href="https://canastarosa.invisionapp.com/console/share/9V1IZIQZSA/480593168" target="_blank">here</a>.

<img src="docs-images/prototype.png"/>

## Some tips

- Keep everything in spaces accorind to your products for example Marketplace Web and Marketplace APP.
- Tag the state of each design, this way anyone can see the state of that design easily.

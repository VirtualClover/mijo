<img src="https://res.cloudinary.com/dxp9idlag/image/upload/c_fill/v1512611882/nxjwdgxi2ftn5qc6wd8a.png" class="img-flat" width="100" />
# The Craft plugin

MIJO's framework makes use of InVision's craft plugin, it is a powerful tool for wireframing. Below we list some of the funcionality that the MIJO framework uses and a little description of them.

## DATA

Craft's DATA function let's you test your design with real data from a JSON file, you can add a local file on consult one from and enpoint.

<img src="docs-images/craft-data.png" />

## Duplicate

Duplicate let's you generate grids from a component, it has some issues when using with symbols, but useful nonetheless.

<img src="docs-images/craft-duplicate.png" />

## Sync

With Sync you can upload your design to you InVision for handoff, you can select a or create a space and give it a device for testing.

<img src="docs-images/craft-sync.png" />

# File structure

To make file navigation easy to find MIJO recommends a putting the files under the product that you are currently working on, with the following architecture:

```
/Product
    /Product--type--name
        /resources(Images, icons and such)
        product--type--name.sketch

```

For example, let's say we are working on a new menu for the marketplace, we would structured it like this:

```
/MarketPlace
    /MarketPlace--feature--newMenu
        /resources(Images, icons and such)
        MarketPlace--feature--newMenu.sketch

```

<br />
## Naming convetions

The MIJO framewrok recommends using the following naming convetion for folders and files to make it easy to understand the contetn of said file.

`Product--type--name`

So, following our example of the new menu:

`MarketPlace--feature--newMenu`

<img src="https://www.getstark.co/avatars/team-stark.png" class="img-flat" width="100" height="100"/>

# The Stark plugin

MIJO's framework makes use of the Stark plugin to make sure our interfaces meet the accessibility standards. You can donwload it from <a href='https://www.getstark.co/' target='_blank'>here</a>.  
For more info on accesibility, please visit the <a href="https://www.a11yproject.com/" target="_blank">A11Y accessibility project site</a>.

## Contrast test

All wireframes and elements submitted to MIJO need to pass at least the contrast test.
To excecute a contras test, first you'll need to select a background and foreground layer.

<img src="docs-images/stark-1.png" />

Then select **Check contrast** from the stark plugin menu or press ⇧⌘P.

<img src="docs-images/stark-2.png" />

This will prompt a screen detailing the contrast test results, MIJO recommends aiming for an AA score, also keep an eye on the ratio.

<img src="docs-images/stark-3.png" />

# Getting Started | UI Design

Here's a basic guide to get started designing interfaces with the MIJO framework and methodology.

## 1. Install sketch

First things first, it is imponrtant to mention that MIJO's UI design revolves around sketch.
You can get sketch <a href='https://www.sketch.com/' target='_blank'>from here</a>.

## 2. Install our recommended sketch plugins

Our design process occupies 2 major plugins:

- <a href='https://www.invisionapp.com/craft' target='_blank'>The Invison's Craft plugin</a>
- <a href='https://www.getstark.co/' target='_blank'>The Stark plugin.</a>

Theres also guides for these plugins on the **Sketch plugins** section of this documentation.

## 3. Install Kactus

For control versioning the MIJO framework uses kactus, you can download it from <a href="https://kactus.io/" target="_blank">here</a>.
There's is also a quick guide for using kactus on the **Sketch practices** section of this documentation.

After that you are all set, we recommend further reading of this section to undrestand in more depth MIJO's UI workflow.

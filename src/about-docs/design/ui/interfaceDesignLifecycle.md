# UI Design Lifecycle

Mijo framework has 4 steps for a successful design process.

## 1. Wireframing & prototyping

After the UX planning process we start designing with the requierements we need.

Starting with low and medium fidelity, these are then reviewed by the team, either by a supervisor review or a peer review process to spot further problems and adjustments. After thst you are all ready and set to go into high fidelity.  
You can read further about these steps under the **Sketch practices** section of this documentation.

## 2. Uploading to the Invision platform

The InVision platform allows us to share wireframes and specs about them for usability tests and handoff.
We recommend the use of tags and spaces within of the platform to keep track of the current stat of our projects.

## 3. Usability testing

After our prototype and/or wireframe is up on InVision we can then put it to test, MIJO uses maze to test most the designs but occasionally we do 1 on 1 with our users if the design requieres more specific or sensible feedback.

## 4. Adjustments

Once we have the results of our usability test we can make adjustments to our design if needed. And, if it's requiered, a second round of usability tests.

## 5. Handoff

When our design pass our tests, then we can share them with our developers to start the development process.

## 6. QA

Once the design is implemented in code UI designers along with UX designers need to give it a last test drive to see if there final product accomplish all the requierements layed down on the previous processes.

After that UX designers took it from here, continuing with the release plan and post-deploy processes.

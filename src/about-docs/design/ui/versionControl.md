# Sketch file's version control

The MIJO framework uses <a href="https://kactus.io/" target="_blank">Kactus</a> for its version control, here's a quick guide on how to set it up.

## 1. Download & install Kactus

You can download Kactus from <a href="https://github.com/kactus-io/kactus/releases/latest/download/Kactus-macos.zip" target="_blank">here</a>.

Once donwloaded, run the instalation and open it.
The first time you open Kactus you will need to sign in with a <a href="https://github.com/join" target="_blank">GitHub account</a>, next it will suggest you to take a tutorial, I highly recommend to do it, takes a few minutes.
Upon finishing the tutorial you will be promp with this screen.

<img src="docs-images/kactus_open.png" />

Next we will link our repositories.

## 2. Adding repositories

On the top right of the window you will find the repository options, once yo click it will display a menu where you can create/clone or upload the repositories you need.

<img src="docs-images/kactus-repositories.png" />

Once added, your local repositories will appear in this menu, select the repo you've just donwloaded.

## 2. Creating a branch

We can create branches from existing repositories to make it easy to make changes without compromising the mastr branch.
To create a branch you can click on the branches button on the top menu and select **Add branch**.

<img src="docs-images/kactus-branches.png" />

## 3. Editing a sketch file from the repo

The way Kactus works is very similar to Git, you can create branches, edit files, commit changes and upload them to the repository.

If you are not familiar with Git i highly reccomend taking this <a href="https://lab.github.com/githubtraining/introduction-to-github" target="_blank">quick course</a> to get the gist of it.

To edit a sketch file in the repository, you can open it from the finder like normal or simply select the file in Kactus from the menu that is just under the repositories button and click **Open File**.

<img src="docs-images/kactus-open-sketch.png" />

## 4. Uploading your changes

Once you made the changes necessaries to a file, kactus will detect them and show the to you uder the list of sketch files. you can click on any of this changes to see a side by side comparison of the original file and the new one.

<img src="docs-images/kactus-compare-changes.png" />

> You can discard any changes right clicking on them and selecting **Discard changes**.

Just like Git, we need to commit the changes before uploading them to origin. the bottom-most menu on the left its our commit panel, once we are satisfied with the changes made, we add the summary of the commit and if we want, we can add a more detail description of the changes.

After we commited our changes we can click on the branch menu and select **Push origin**.

<img src="docs-images/kactus-push.png" />

If want to make a **pull request** given that we cannot push to master, you can do so on the **branches menu**.

...And that's it, we added a repo, created a branch, edited a file and uploaded said file to the repo!

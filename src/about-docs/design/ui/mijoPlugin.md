# The MIJO's plugin

MIJO's framework has its own plugin containing existing elements from our design system.
You can download the latest version <a href="http://virtualclover.gitlab.io/mijo/feature-babel/public/downloads/mijoDS.sketchplugin.zip">here</a>.

Once it is donwloaded you can simply select it from the Plugin menu in sketch.

> WARNING: Opening MIJO's Skecth kit will overwrite your current open document, so we recommend creating a new file and using the plugin, then, when you get the components you want, you can erase it.

<img src="docs-images/mijo-plugin.png" />

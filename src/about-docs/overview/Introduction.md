<img src="docs-images/cover.jpg" class="img-flat">

# Welcome to MIJO

This is Canasta Rosa's design framework. It has been built to support our teams of designers and developers across Canasta Rosa in creating best in class digital products and experiences.

This framework consists of: our design principles, design and development guidelines, methodologies, styleguides, and working code. It is maintained by our core design system managers and continually improved by our amazing community of contributors.

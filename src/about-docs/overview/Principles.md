# MIJO Principles

Regardless of what you are building with this design system, your goals and outcomes should align with the following principles and purposes:

## We create trust.

Our goal is to build reliable products and services that help our customers to succeed.

## We design for real users.

Our design process isn't just focused on building products for people, it's aimed at creating them with people. This means we assume we don't know every real user's need or desire and are continually testing and learning to continuously adding value to our products.

## We build consistency.

Consistency is how we align on our values, share our learnings, and design using a common language. Every component was created to work together to help create a consistent customer experience; each touchpoint should feel familiar and meet their needs in the simplest way possible.

## We design for flexibility.

Every user is different and so is the device and their internet access. The foundation of our system has been designed and built to gracefully adjust to these factors to always offer the best experience possible.

## We design for the future.

Our industry is always changing so our tools and framework might not be the same in 3,5, or 10 years, our system needs to be capable of adapt to different technologies, frameworks and tools so what we built today will be the pillars of our future.

# Bold

This is a variant of paragraph. Generally used for resalting some text in a paragraph or to indicate a link.

## Specs

| Spec type         | Spec     |
| ----------------- | -------- |
| Font family       | Sarabun  |
| Font size         | 16       |
| Line Height       | 18       |
| Font weight       | Semibold |
| Recommended color | dark200  |

## Use Example

<img src='docs-images/bold.png' />

# Typography

Typography is a major part of Canasta Rosa's brand. We've taken care to select a family of fonts that promote legibility and accessibility.

## Fonts

### Sarabun

We use Sarabun for all the text on the site and most of our media.  
<a href='https://fonts.google.com/download?family=Sarabun'>Download</a>

### Museo

Museo is the font of our logo, don't use it in paragraphs, limited to titles on social media only,  
<a href='https://fonts.google.com/download?family=Museo'>Download</a>

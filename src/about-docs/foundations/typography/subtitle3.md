# Subtitle 3

The smallest of the subtitles. Generally used for subsections titles.

## Specs

| Spec type         | Spec     |
| ----------------- | -------- |
| Font family       | Sarabun  |
| Font size         | 18       |
| Line Height       | 20       |
| Font weight       | Semibold |
| Recommended color | dark200  |

## Use Example

<img src='docs-images/subtitle3.png' />

# Paragraph

The standard text style. Generally used for bodies of text.

## Specs

| Spec type         | Spec    |
| ----------------- | ------- |
| Font family       | Sarabun |
| Font size         | 16      |
| Line Height       | 18      |
| Font weight       | Regular |
| Recommended color | dark200 |

## Use Example

<img src='docs-images/paragraph.png' />

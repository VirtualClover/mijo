# Caption

The second smallest text. As the name implies this one is used for things like captions and such.

> Fonts this size or smaller sometimes can cause eyestrain specially in bodies of text, do not over use it!

## Specs

| Spec type         | Spec    |
| ----------------- | ------- |
| Font family       | Sarabun |
| Font size         | 13      |
| Line Height       | 15      |
| Font weight       | Regular |
| Recommended color | dark200 |

## Use Example

<img src='docs-images/caption.png' />

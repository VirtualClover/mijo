# Subtitle 2

This is the third highest hierarchy style of text. Generally used for title of sections.

## Specs

| Spec type         | Spec     |
| ----------------- | -------- |
| Font family       | Sarabun  |
| Font size         | 22       |
| Line Height       | 24       |
| Font weight       | Semibold |
| Recommended color | dark200  |

## Use Example

<img src='docs-images/subtitle2.png' />

# Title

This is the highest hierarchy style of text, use only in page titles.

## Specs

| Spec type         | Spec     |
| ----------------- | -------- |
| Font family       | Sarabun  |
| Font size         | 36       |
| Line Height       | 38       |
| Font weight       | Semibold |
| Recommended color | dark300  |

## Use Example

<img src='docs-images/title.png' />

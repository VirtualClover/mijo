# Subtitle

This is the second highest hierarchy style of text, used as an extension of title and also for page titles when the title is too much.

## Specs

| Spec type         | Spec     |
| ----------------- | -------- |
| Font family       | Sarabun  |
| Font size         | 26       |
| Line Height       | 28       |
| Font weight       | Semibold |
| Recommended color | dark200  |

## Use Example

<img src='docs-images/subtitle.png' />

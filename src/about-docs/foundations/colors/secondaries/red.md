# Red

Red is used for higlighting errors.

## Specs

| Shade  | Hex     | RGB           | HSL          | Recommended text color to pair with | Notes |
| ------ | ------- | ------------- | ------------ | ----------------------------------- | ----- |
| red100 | #FFCCCC | 255, 204, 204 | 0, 100%, 90% | Dark                                |       |
| red200 | #E76E6E | 231, 110, 110 | 0, 72%, 67%  | White                               |       |
| red300 | #CC3131 | 204, 49, 49   | 0, 61%, 50%  | White                               |       |
| red400 | #960400 | 150, 4, 0     | 2, 100%, 29% | White                               |       |

## Use examples

<img src='docs-images/redExample.png'/>

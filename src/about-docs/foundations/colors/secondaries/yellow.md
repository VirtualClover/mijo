# Yellow

These shades are used for higlighting warnings or notices.

> For example, this one!

## Specs

| Shade     | Hex     | RGB           | HSL           | Recommended text color to pair with | Notes |
| --------- | ------- | ------------- | ------------- | ----------------------------------- | ----- |
| yellow100 | #FFFBF0 | 255, 251, 240 | 44, 100%, 97% | Dark                                |       |
| yellow200 | #f8efc0 | 248, 239, 192 | 50, 80%, 86%  | Dark                                |       |
| yellow300 | #f8db6a | 248, 219, 106 | 48, 91%, 69%  | Dark                                |       |
| yellow400 | #8f7d1d | 143, 125, 29  | 51, 66%, 34%  | White                               |       |

## Use examples

<img src='docs-images/yellowExamples.png'/>

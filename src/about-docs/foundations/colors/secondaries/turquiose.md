# Turquiose

These shades are used for higlighting positive things like discounts or successes.

## Specs

| Shade        | Hex     | RGB           | HSL            | Recommended text color to pair with | Notes |
| ------------ | ------- | ------------- | -------------- | ----------------------------------- | ----- |
| turquiose100 | #C0FFEE | 192, 255, 238 | 164, 100%, 88% | Dark                                |       |
| turquiose200 | #5BC3A7 | 91, 195, 167  | 164, 46%, 56%  | Dark                                |       |
| turquiose300 | #1DB592 | 29, 181, 146  | 166, 72%, 41%  | White                               |       |
| turquiose400 | #217856 | 33, 120, 86   | 157, 57%, 30%  | White                               |       |

## Use examples

<img src='docs-images/turquioseExample.png'/>

# Dark

Dark is mainly used in our text.

> Please try to limit dark text colors to this shade (with the exception of gray400), since lighter colors can cause legibility colors.

## Specs

| Shade   | Hex     | RGB           | HSL          | Recommended text color to pair with | Notes                                                                                  |
| ------- | ------- | ------------- | ------------ | ----------------------------------- | -------------------------------------------------------------------------------------- |
| dark100 | #6b6b7a | 107, 107, 122 | 240, 7%, 45% | White                               | Not counting input placeholders, this is the lighter dark text color, our text can be. |
| dark200 | #494953 | 73, 73, 83    | 240, 6%, 31% | White                               | This is subtitle 3 and smaller text sizes default color.                               |
| dark300 | #37373e | 55, 55, 62    | 240, 6%, 23% | White                               | This is title and subtitle's default text color.                                       |
| dark400 | #2b2b30 | 43, 43, 48    | 240, 5%, 18% | White                               |                                                                                        |

## Use examples

<img src='docs-images/darkExamples.png'/>

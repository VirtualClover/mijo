# Main

This is the primary shade of our design framework.
It is used to stablish our brand a to highlight elements like links or buttons.

## Specs

| Shade   | Hex     | RGB           | HSL           | Recommended text color to pair with | Notes                                  |
| ------- | ------- | ------------- | ------------- | ----------------------------------- | -------------------------------------- |
| main100 | #f6cdd3 | 246, 205, 211 | 351, 69%, 88% | Dark                                |                                        |
| main200 | #e67787 | 230, 119, 135 | 339, 75%, 90% | White                               |                                        |
| main300 | #e63976 | 230, 57, 118  | 339, 78%, 56% | White                               | This is Canasta Rosa's flagship color. |
| main400 | #d83970 | 216, 57, 112  | 339, 67%, 54% | White                               |                                        |

## Use examples

<img src='docs-images/mainExamples.png'/>

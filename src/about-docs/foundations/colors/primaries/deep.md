# Deep

As of MIJO 1.1.0 This are the complementary shades of our main one. Please use clear text colors if you are using this as a background.  
These are shades used in the navigation bar at Canasta Rosa.

## Specs

| Shade   | Hex     | RGB           | HSL           | Recommended text color to pair with | Notes                                             |
| ------- | ------- | ------------- | ------------- | ----------------------------------- | ------------------------------------------------- |
| deep100 | #8080A5 | 128, 128, 165 | 240, 17%, 57% | White                               |                                                   |
| deep200 | #34344b | 52, 52, 75    | 240, 18%, 25% | White                               | This is the default color of our navigation bars. |
| deep300 | #21212C | 33, 33, 44    | 240, 14%, 15% | White                               | This is Canasta Rosa's complementary color        |
| deep400 | #161620 | 22, 22, 32    | 240, 19%, 11% | White                               |                                                   |

## Use examples

<img src='docs-images/deepExamples.png'/>

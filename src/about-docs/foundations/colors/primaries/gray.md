# Gray

Gray is often use for marking borders within layouts.

> If you are working with light text colors these are the recommended shades to use up until gray 200.

## Specs

| Shade   | Hex     | RGB           | HSL         | Recommended text color to pair with | Notes                                                                                                                                                          |
| ------- | ------- | ------------- | ----------- | ----------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| gray100 | #f1f1f1 | 241, 241, 241 | 0°, 0%, 95% | Dark                                | As of MIJO 1.1 this the oficial background color for containers.                                                                                               |
| gray200 | #dad8d6 | 218, 216, 214 | 30, 5%, 85% | Dark                                | This is the default color for borders and separators.                                                                                                          |
| gray300 | #b7b7b7 | 183, 183, 183 | 0, 0%, 72%  | Dark                                |                                                                                                                                                                |
| dark400 | #898989 | 137, 137, 137 | 0, 0%, 54%  | White                               | This is the color used for placeholder text on inputs, please don't use colors lighter than this for placeholder text, since it can cuase legibility problems. |

## Use examples

<img src='docs-images/grayExamples.png'/>

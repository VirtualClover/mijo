# Creating a component story

Component stories are a group stories formed by a father story(Our component) and various child stories (Our component variations).

## 1. Create a new markdown file.

When creating a component story its documentation should be in markdown file on the folder of the component, and it has to be called README.md for example if we are creating a story about the component CRButton, it would look like this:

    /CRbutton
        |--CRButton.js
        |--index.js
        |--style.js
        |--README.md

This way the documentation can also be displayed in the repo.

## 2 Create a .stories.js file

Once you have your mark down file with your documentation you can create a story, stories are stored on the **~/src/stories** folder of the project, the name of the file can be whatever (preferably aligned to the component or documentation that you are creating) and the extension has to be ".stories.js" since we are adding react components.

Once created the file we import the needed libraries,the README file and the component or components realted to the story.

```
import React from "react";
import docs from "../../../core/react-native/CRbutton/README.md";
import { CRButton } from "../../../core/react-native/CRButton/CRButton";

```

> We need to import the component from the it's .js file NOT from an index, since this can cause a bug within storybook.

### Adding meta information

Our parent story then needs its meta information, in the next example we are adding a title, a description and specifying the component of the story.

```
export default {
  title: "React Native/Components/CRButton",
  component: CRButton,
  parameters: {
    docs: {
      description: {
        component: docs,
      },
    },
  },
};
```

If you want to know more about this export you can visit <a href='https://storybook.js.org/docs/react/writing-stories/introduction#default-export' tagret='_blank'>this link </a>

### Creating our child story/stories

Once we have the meta information of our parent story we can start creating component specific stories, let's say we want to add a child story about the default state of the CRButton component with the prop **children** with a value of **"Default"**:

```
const Comp = (args) => <CRButton {...args} />;
export const Default = Comp.bind({});
Default.args = {
  children: "Default",
};

```

Then we should se something like this:

<img src='docs-images/cr_button--default.png' />

We can add more child stories to represent the variations, let's say we want to add the **"Ghosted"** variation of the button.

```
export const Pink_Ghosted = Comp.bind({});
Pink_Ghosted.args = {
  variant: "pinkGhost",
};
```

Then we should se something like this:

<img src='docs-images/cr_button--pinkGhost.png' />

if you want to know more about constructing stories you can visit <a href='https://storybook.js.org/docs/react/writing-stories/introduction' tagret='_blank'><strong>this link</strong></a>.

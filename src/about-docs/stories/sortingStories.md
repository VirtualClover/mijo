# Sorting our stories

Sotrybook will automatically sort the stories in alfabetical order, however, it is quite primitve, if we want to control the sorting of the sotries we can do so in the **preview.js** file in the **.storybook** folder, but sadly we can only sort 2 levels at the moment, we sort them like javascript objects, for example, if we want that the Components folder be displayed first and CRbutton be the first child displayed within this folder we can do this:

```
 const config = [
        {
          category: "Components",
          order: ["CRButton", "CRCard"],
        },

         {
          category: "Other category",
          order: ["Docs"],
        },

```

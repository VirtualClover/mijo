# Stories

Stories are an entry on the documentation to put it simple, they describe us various elements of the DS, like components guidelines, manuals, etc.

<img src='docs-images/stories.png' />

There are 2 types of stories althoug similar, each type has it's own way of creating a story:

- [**Documentation-only story**]('/?path=/story/stories-writing-documentation-only-stories--page')  
   Like this one you're seeing rght now
- [**Component story**]('/?path=/story/stories-writing-component-stories--page')  
   A story with a live component embeded within

The following stories contain little guides on creating both types.

There's also template stories that you can use in the <a href="https://gitlab.com/VirtualClover/mijo/-/tree/master/src/stories/templates" target="_blank">**project**</a>.

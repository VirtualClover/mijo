# Creating a documentation only story.

## 1. Create a new markdown file.

When wrtiting a doc-only story it needs to be stored in the **about-docs** folder located in the src folder, it can have any name as long as it is understandable.

Here we can start writing the documentation like any other markdown file.

### Importing images to markdown

All images that we are going to embed in any documentation needs to be stores in the **~/src/about-docs/docs-images** folder, this way we can display them in the repo files and in storybook.

## 2 Create a .stories.mdx file

Once you have your mark down file with your documentation you can create a story, stories are stored on the **~/src/stories** folder of the project, the name of the file can be whatever (preferably aligned to the component or documentation that you are creating) and the extension has to be ".stories.mdx".

For example, if we wanted to create a documentation describing **maintenance** our story file should be name **maintenance.stories.mdx**.
Then we can import our markdown file like this:

`import docs from "../../about-docs/stories/exampleStory.md";`

Every doc-only story file NEEDS to have 2 things at least:

- **A Meta tag**  
  Here we describe the title of the story, if is a nested story it needs to be separated with a **/**, fox example:  
  `<Meta title="Parent section / Example Story" />`

* **A Description tag**  
   Here we will put our markdown file that we created earlier.
  Fox example:  
  `<Description>{docs}</Description>`

If you want to know more about **MDX files** you can look <a href='https://mdxjs.com/' target='_blank'><strong>here</strong></a>.

Additionally if you want to know more about constructing stories you can visit <a href='https://storybook.js.org/docs/formats/mdx-syntax/' tagret='_blank'><strong>this link</strong></a>.

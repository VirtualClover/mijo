/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
import { View } from 'react-native';
//MIJO
import { CRText } from '../CRText';
import { CRError } from '../CRError';
//Style
import { styles } from './CRView.style';
import { colors } from '../../theme/colors/js/variables.style';
//Model
import { model } from './CRView.model';

export const CRView = (props) => {
  try {
    var styleConstructor = [
      props.debug ? [styles.debug, { borderColor: props.debugColor }] : null,
      { backgroundColor: colors[props.backgroundColor] },
      props.style,
    ];
    var childrenConstructor = props.debug ? (
      <View
        key={'CRDEBUGTOOLTIPWRAPPER'}
        style={[styles.tooltip, { backgroundColor: props.debugColor }]}
      >
        <CRText color={'white'}>{props.debugTooltipName}</CRText>
      </View>
    ) : null;
    return (
      <View style={styleConstructor}>
        {childrenConstructor}
        {props.children}
      </View>
    );
  } catch (error) {
    return <CRError component={'CRView'} error={error} />;
  }
};

CRView.propTypes = {
  ...model.types,
};

CRView.defaultProps = {
  ...model.default,
};

/*

This file was created by Canasta Rosa
under the MIT license.

*/

import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  debug: {
    borderWidth: 1,
    borderStyle: 'dashed',
  },
  tooltip: {
    paddingHorizontal: 3,
    alignSelf: 'stretch',
    position: 'absolute',
    top: -21,
  },
  error: {
    backgroundColor: 'red',
  },
  errorText: {
    color: 'white',
  },
});

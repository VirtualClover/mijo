/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    className: PropTypes.string,
    color: PropTypes.string,
    size: PropTypes.number,
  },
  default: {
    className: '',
    color: 'dark300',
    size: '8',
    variant: 'basket',
  },
};

import iconConfig from '../../theme/assets/fonts/canasta_sans/selection.json';
/**
 * Retrives an icon from the font's selection.json given a key
 * @param {String} value The name of the icon that the function will be looking for.
 * @returns a charcode
 */
export function getObject(value) {
  let data = iconConfig.icons;
  let objectOnArray = {};
  data.forEach(function (element) {
    for (let key in element) {
      if (element[key].name == value) objectOnArray = element[key];
    }
  });
  return objectOnArray;
}

/*

This file was created by Canasta Rosa 
under the MIT license.

*/
import React from 'react';
//MIJO
import { CRError } from '../CRError';
import { CRText } from '../CRText';
//Model
import { model } from './CRIcon.model';
//Functionality
import { getObject } from './CRIcon.func';
/**
 * MIJO's base text component
 * @param {Object} props
 */
export const CRIcon = (props) => {
  try {
    const iconObject = getObject(props.variant);
    return (
      <CRText
        variant={'icon'}
        color={props.color}
        style={{ fontSize: props.size }}
        align="center"
      >
        {String.fromCharCode(iconObject.code)}
      </CRText>
    );
  } catch (error) {
    return <CRError component={'CRIcon'} error={error} />;
  }
};

CRIcon.propTypes = {
  ...model.types,
};

CRIcon.defaultProps = {
  ...model.default,
};

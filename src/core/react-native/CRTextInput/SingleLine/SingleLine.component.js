import React, { useState } from 'react';
import { View, TextInput } from 'react-native';
//MIJO
import { CRText } from '../../CRText';
import { CRButton } from '../../../components/CRButton';
import { CRError } from '../../CRError';
import { colors } from '../../../theme/colors';
//Style
import { styles } from './SingleLine.style';
//Model
import { model } from './SingleLine.model';
//Functionality
import { handleChangeText, handleEndEditing } from './SingleLine.func';
/**
 * MIJO's single line text input component
 * @param {Object} props
 */
export const SingleLine = (props) => {
  try {
    //State
    const [actualBorderColor, onChangeBorder] = useState(colors.gray200);
    const [error, setError] = useState(false);
    const [value, setValue] = useState('');

    return (
      <View style={styles.inputWrapper}>
        <View style={[styles.innerWrapper, { borderColor: actualBorderColor }]}>
          <TextInput
            onChangeText={(text) =>
              handleChangeText(
                text,
                props.validation,
                props.onChangeText,
                setValue,
                setError,
                onChangeBorder
              )
            }
            value={value}
            style={[styles.input, props.style]}
            placeholder={props.placeholder}
            placeholderTextColor={colors.gray400}
            onEndEditing={() => handleEndEditing(onChangeBorder)}
            onFocus={() => onChangeBorder(colors.blue200)}
            secureTextEntry={props.secureTextEntry}
            maxLength={props.maxLength}
            editable={props.editable}
            autoCapitalize={props.autoCapitalize}
            autoCompleteType={props.autoCompleteType}
            keyboardType={props.keyboardType}
          />
          {props.tooltipIcon ? (
            <CRButton.Icon
              iconVariant={props.tooltipIcon}
              color={'transparent'}
              textColor={'dark300'}
              iconSize={16}
            />
          ) : null}
        </View>
        {error ? (
          <View style={styles.errorWrapper}>
            <CRText color={'red300'} variant={'caption'}>
              {props.errorMessage}
            </CRText>
          </View>
        ) : null}
        {!props.editable ? <View style={styles.nonEditableOverlay} /> : null}
      </View>
    );
  } catch (error) {
    return <CRError component={'CRTextInput.SingleLine'} error={error} />;
  }
};
SingleLine.propTypes = {
  ...model.types,
};

SingleLine.defaultProps = {
  ...model.default,
};

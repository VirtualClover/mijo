/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    placeholder: PropTypes.string.isRequired,
    onChangeText: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
    secureTextEntry: PropTypes.bool.isRequired,
    validation: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string,
    maxLength: PropTypes.number.isRequired,
    editable: PropTypes.bool.isRequired,
    autoCapitalize: PropTypes.oneOf([
      'none',
      'sentences',
      'words',
      'characters',
    ]).isRequired,
    autoCompleteType: PropTypes.oneOf([
      'off',
      'username',
      'password',
      'email',
      'name',
      'tel',
      'street-address',
      'postal-code',
      'cc-number',
      'cc-csc',
      'cc-exp',
      'cc-exp-month',
      'cc-exp-year',
    ]).isRequired,
    tooltipOnPress: PropTypes.func,
    tooltipIcon: PropTypes.string,
  },
  default: {
    placeholder: 'SAMPLE PLACEHOLDER',
    onChangeText: () => {},
    value: '',
    secureTextEntry: false,
    errorMessage: 'SAMPLE_ERR',
    validationType: true,
    maxLength: 90,
    editable: true,
    autoCapitalize: 'sentences',
    autoCompleteType: 'off',
    tooltipIcon: '',
  },
};

/*

This file was created by Canasta Rosa
under the MIT license.

*/

/********** Functionality of this component ************/
import { colors } from '../../../theme/colors';
/**
 * Handler for every time the text change on the input
 * @param {String} text The text return from the arrow function
 * @param {Boolean} validation A boolean to determine if the input's validation is correct
 * @param {function onChangeText() {}} onChangeText A function that can be passed to excecute when there's a change in the input
 * @param {function setValueHook(state) {}} setValueHook A hook to set the value of the component
 * @param {function setErrorHook(state) {}} setErrorHook A hook to set the error of the component
 * @param {function onChangeBorderHook(state) {}} onChangeBorderHook A hook to change the border color of the input's border.
 */
export function handleChangeText(
  text,
  validation,
  onChangeText,
  setValueHook,
  setErrorHook,
  onChangeBorderHook
) {
  onChangeText(text);
  setValueHook(text);
  if (validation) {
    setErrorHook(false);
    onChangeBorderHook(colors.gray200);
  } else {
    setErrorHook(true);
    onChangeBorderHook(colors.red300);
  }
}
/**
 * Handler called when the user has clicked/tapped away from the input
 * @param {function onChangeBorderHook(state){}} onChangeBorderHook A hook to change the border color of the input's border.
 */
export function handleEndEditing(onChangeBorderHook) {
  onChangeBorderHook(colors.gray200);
}

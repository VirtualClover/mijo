import { StyleSheet } from 'react-native';
import { colors } from '../../../theme/colors';

export const styles = StyleSheet.create({
  inputWrapper: {
    marginBottom: 10,
    height: 35,
  },
  innerWrapper: {
    borderRadius: 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colors.white,
    borderWidth: 1,
    width: '100%',
  },
  input: {
    color: colors.dark300,
    fontSize: 16,
    paddingHorizontal: 5,
    height: 35,
    //backgroundColor: 'red',
    flex: 15,
  },
  tooltip: {
    paddingHorizontal: 7,
  },
  errorWrapper: {
    marginTop: 5,
    marginBottom: 10,
  },
  nonEditableOverlay: {
    opacity: 0.5,
    backgroundColor: colors.gray200,
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
});

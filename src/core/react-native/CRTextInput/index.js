/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import { SingleLine } from './SingleLine/SingleLine.component';

export const CRTextInput = {
  SingleLine,
};

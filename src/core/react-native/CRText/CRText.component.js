/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
import { Text } from 'react-native';
//Model
import { model } from './CRText.model';
//MIJO
import { colors } from '../../theme/colors/js/variables.style';
import { textStyles } from '../../theme/typography/js/variables.style';
import { CRError } from '../CRError';
/**
 * CRText
 * Base text component of the MIJO DS
 * @see <a href="https://virtualclover.gitlab.io/mijo/master/?path=/story/%F0%9F%A4%96-components-%F0%9F%96%8A%EF%B8%8F-typography-crtext--title">CRText documentation</a>
 */
export const CRText = (props) => {
  try {
    if (props.children.length > props.limit) {
      var finalText = props.children.substring(0, props.limit) + '...';
    } else {
      finalText = props.children;
    }

    return (
      <Text
        style={[
          textStyles[props.variant],
          { color: colors[props.color] },
          { textAlign: props.align },
          props.style,
        ]}
        name={`CRText`}
      >
        {finalText}
      </Text>
    );
  } catch (error) {
    return <CRError component={'CRText'} error={error} />;
  }
};

CRText.propTypes = {
  ...model.types,
};

CRText.defaultProps = {
  ...model.default,
};

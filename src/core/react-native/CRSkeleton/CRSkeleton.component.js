/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React, { useRef, useEffect } from 'react';
import { Animated, Easing } from 'react-native';
//MIJO
import { CRImg } from '../CRImg';
//Style
import { styles } from './CRSkeleton.style';
import { CRError } from '../CRError';
//Model
import { model } from './CRSkeleton.model';
import { SkeletonImage } from './Image/CRSkeleton.Image.component';
/**
 * MIJO's skeleton component.
 * @param {Object} props
 */
export const CRSkeleton = (props) => {
  try {
    var fadeAnim = useRef(new Animated.Value(1)).current; // Initial value for opacity: 0
    useEffect(() => {
      Animated.loop(
        Animated.sequence([
          Animated.timing(fadeAnim, {
            toValue: 0.5,
            duration: 1000,
            useNativeDriver: true,
            easing: Easing.in,
          }),
          Animated.timing(fadeAnim, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
            easing: Easing.in,
          }),
        ])
      ).start();
    });
    const skeletonVariant = {
      image: (
        <Animated.View style={{ opacity: fadeAnim }}>
          <CRImg resizeMode={props.resizeMode} style={props.style} />
        </Animated.View>
      ),
      block: (
        <Animated.View
          style={[
            styles.base,
            styles.block,
            { opacity: fadeAnim },
            props.style,
          ]}
        />
      ),
      text: (
        <Animated.View
          style={[styles.base, styles.text, { opacity: fadeAnim }, props.style]}
        />
      ),
      title: (
        <Animated.View
          style={[
            styles.base,
            styles.title,
            { opacity: fadeAnim },
            props.style,
          ]}
        />
      ),
    };
    var skeletonVariantID = skeletonVariant[props.variant];
    return skeletonVariantID;
  } catch (error) {
    return <CRError component={'CRSkeleton'} error={error} />;
  }
};

CRSkeleton.Image = SkeletonImage;

CRSkeleton.propTypes = {
  ...model.types,
};

CRSkeleton.defaultProps = {
  ...model.default,
};

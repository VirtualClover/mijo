/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React, { useRef, useEffect } from 'react';
import { Animated, Easing } from 'react-native';
//MIJO
import { CRImg } from '../../CRImg';
//Style
import { styles } from '../CRSkeleton.style';
import { CRError } from '../../CRError';
//Model
import { model } from '../CRSkeleton.model';
/**
 * MIJO's skeleton component.
 * @param {Object} props
 */
export const SkeletonImage = (props) => {
  try {
    var fadeAnim = useRef(new Animated.Value(1)).current; // Initial value for opacity: 0
    useEffect(() => {
      Animated.loop(
        Animated.sequence([
          Animated.timing(fadeAnim, {
            toValue: 0.5,
            duration: 1000,
            useNativeDriver: true,
            easing: Easing.in,
          }),
          Animated.timing(fadeAnim, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
            easing: Easing.in,
          }),
        ])
      ).start();
    });
    return (
      <Animated.View style={{ opacity: fadeAnim }}>
        <CRImg resizeMode={props.resizeMode} style={props.style} />
      </Animated.View>
    );
  } catch (error) {
    return <CRError component={'CRSkeleton'} error={error} />;
  }
};

SkeletonImage.propTypes = {
  ...model.types,
};

SkeletonImage.defaultProps = {
  ...model.default,
};

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import { StyleSheet } from "react-native";
import { colors } from "../../theme/colors/js/variables.style";
export const styles = StyleSheet.create({
  base: {
    backgroundColor: colors.gray300,
    width: "100%",
  },
  block: {
    minHeight: 40,
    borderRadius: 3,
  },
  text: {
    height: 16,
    borderRadius: 10,
  },
  title: {
    height: 25,
    borderRadius: 20,
  },
});

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
  TouchableHighlight,
} from 'react-native';
//MIJO
import { CRError } from '../CRError';
import { CRText } from '../CRText';
//Model
import { model } from './CRAction.model';
/**
 * Base action component of the MIJO DS
 * @param {Object} props
 */
export const CRAction = (props) => {
  try {
    const actionTag = {
      button: [
        <TouchableOpacity
          key={props.variant}
          onPress={props.onPress}
          style={props.style}
          disabled={props.disabled}
        >
          <CRText
            color={props.textColor}
            align={props.textAlign}
            style={props.textStyle}
          >
            {props.children ? props.children : props.value}
          </CRText>
        </TouchableOpacity>,
      ],
      touchableOpacity: [
        <TouchableOpacity
          key={props.variant}
          onPress={props.onPress}
          style={props.style}
          disabled={props.disabled}
        >
          <CRText
            color={props.textColor}
            align={props.textAlign}
            style={props.textStyle}
          >
            {props.children ? props.children : props.value}
          </CRText>
        </TouchableOpacity>,
      ],
      touchableHighlight: [
        <TouchableHighlight
          key={props.variant}
          onPress={props.onPress}
          style={props.style}
          disabled={props.disabled}
        >
          <CRText
            color={props.textColor}
            align={props.textAlign}
            style={props.textStyle}
          >
            {props.children ? props.children : props.value}
          </CRText>
        </TouchableHighlight>,
      ],
      touchableWithoutFeedback: [
        <TouchableWithoutFeedback
          key={props.variant}
          disabled={props.disabled}
          value={props.children}
          onPress={props.onPress}
          style={props.style}
        >
          <CRText
            color={props.textColor}
            align={props.textAlign}
            style={props.textStyle}
          >
            {props.children ? props.children : props.value}
          </CRText>
        </TouchableWithoutFeedback>,
      ],
    };
    return actionTag[props.variant];
  } catch (error) {
    return <CRError component={'CRAction'} error={error} />;
  }
};

CRAction.propTypes = {
  ...model.types,
};

CRAction.defaultProps = {
  ...model.default,
};

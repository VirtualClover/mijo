/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number,
      PropTypes.array,
    ]),
    className: PropTypes.string,
    source: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    placeholderImg: PropTypes.string,
    resizeMode: PropTypes.string,
    alt: PropTypes.string,
  },
  default: {
    style: {},
    className: '',
    source: '/',
    placeholderImg: '',
    resizeMode: 'contain',
    alt: '',
  },
};

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React, { useState } from 'react';
import { Image } from 'react-native';
//MIJO
import placeholderDefault from '../../theme/assets/images/placeholder/placeholder.jpg';
import { CRError } from '../CRError';
//Style
import { styles } from './CRImg.style';
//Model
import { model } from './CRImg.model';
//Functionality
import { useFallbackImg } from './CRImg.func';
/**
 * Base image component of the MIJO DS
 * @param {Object} props
 */
export const CRImg = (props) => {
  try {
    //State
    const [source, setImg] = useState(props.source);

    var finalPlaceholder = props.placeholderImg
      ? props.placeholderImg
      : placeholderDefault;
    const srcProps = useFallbackImg(source, setImg, finalPlaceholder);
    var styleConstructor = [
      styles.base,
      { resizeMode: props.resizeMode },
      props.style,
    ];
    return <Image style={styleConstructor} {...srcProps} />;
  } catch (error) {
    return <CRError component={'CRImg'} error={error} />;
  }
};

CRImg.propTypes = {
  ...model.types,
};

CRImg.defaultProps = {
  ...model.default,
};

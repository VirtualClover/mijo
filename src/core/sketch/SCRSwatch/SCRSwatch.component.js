/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import * as React from "react";
//DS
import { CRText } from "../../components/CRText";
import { CRView } from "../../components/CRView";
import { CRSpace } from "../../components/CRSpace";
//Style
import { styles } from "./SCRSwatch.style";

export const SCRSwatch = ({ color, colorName, colorDesc }) => {
  return (
    <CRView style={styles.wrapper} name={"Swatch"}>
      <CRView
        style={[styles.swatch, { backgroundColor: color }]}
        name={"Shade color"}
      />
      <CRSpace.Vertical variant={"xSmall"} />
      <CRText variant={"bold"}>{colorName}</CRText>
      <CRView style={styles.hex} name={"Hex tag"}>
        <CRText variant={"caption"} color={"dark100"} align={"center"}>
          {colorDesc}
        </CRText>
      </CRView>
    </CRView>
  );
};

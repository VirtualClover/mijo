/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import { StyleSheet } from "react-primitives";
import { colors } from "../../theme/colors";

export const styles = StyleSheet.create({
  swatch: {
    width: 120,
    height: 120,
  },
  hex: {
    marginTop: 2,
    paddingVertical: 2,
    paddingHorizontal: 5,
    borderColor: colors.dark100,
    borderRadius: 3,
    borderWidth: 1,
    width: 70,
  },
});

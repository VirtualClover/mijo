/*

This file was created by Canasta Rosa
under the MIT license.

*/

import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  error: {
    backgroundColor: 'red',
    padding: 10,
    borderRadius: 3,
  },
  errorText: {
    color: 'white',
  },
});

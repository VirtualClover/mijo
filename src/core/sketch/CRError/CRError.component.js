/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
import { View, Text } from 'react-sketchapp';
//Style
import { styles } from './CRError.style';
//Model
import { model } from './CRError.model';
export const CRError = (props) => {
  try {
    console.error(`${props.component}: ${props.error}`);
    return (
      <View style={styles.error}>
        <Text style={styles.errorText}>!ERR/{props.component}</Text>
      </View>
    );
  } catch (error) {
    console.error(error);
    return (
      <View style={styles.error}>
        <Text style={styles.errorText}>!ERR/VIEW</Text>
      </View>
    );
  }
};

CRError.propTypes = {
  ...model.types,
};

CRError.defaultProps = {
  ...model.default,
};

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from "react";
import PropTypes from "prop-types";
//MIJO
import { CRView } from "../CRView";

//Style
import { styles } from "./CRSkeleton.style";
import { CRImg } from "../CRImg";

export const CRSkeleton = ({
  opacity,
  className,
  variant,
  height,
  width,
  style,
  placeholderImg,
  alt,
}) => {
  try {
    const skeletonVariant = {
      image: (
        <CRView>
          <CRImg source={placeholderImg} style={style} />
        </CRView>
      ),
      block: <CRView style={[styles.base, styles.block, style]} />,
      text: <CRView style={[styles.base, styles.text, style]} />,
      title: <CRView style={[styles.base, styles.title, style]} />,
    };
    var skeletonVariantID = skeletonVariant[variant];
    return skeletonVariantID;
  } catch (error) {
    console.log(error);
  }
};

CRSkeleton.propTypes = {
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  opacity: PropTypes.number,
  variant: PropTypes.oneOf(["text", "image", "title", "block"]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  placeholderImg: PropTypes.string,
  alt: PropTypes.string,
};

CRSkeleton.defaultProps = {
  style: {},
  className: "",
  opacity: 1,
  variant: "block",
  height: 100,
  width: "100%",
  placeholderImg: "/",
  alt: "",
};

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number,
      PropTypes.array,
      PropTypes.func,
    ]),
    className: PropTypes.string,
    debug: PropTypes.bool,
    debugTooltipName: PropTypes.string,
    debugTooltipClass: PropTypes.string,
    debugColor: PropTypes.string,
    name: PropTypes.string,
    backgroundColor: PropTypes.string,
  },
  default: {
    style: {},
    className: '',
    debug: false,
    debugTooltipName: 'CRView',
    debugTooltipClass: '',
    debugColor: 'green',
    name: 'CRView',
    backgroundColor: '',
  },
};

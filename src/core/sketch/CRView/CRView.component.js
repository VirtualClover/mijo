/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
import { View } from 'react-sketchapp';
//MIJO
import { CRText } from '../CRText';
import { colors } from '../../theme/colors';
//Styles
import { styles } from './CRView.style';
//Model
import { model } from './CRView.model';
import { CRError } from '../CRError';

/**
 * MIJO's base wrapper component
 * @param {Object} props
 */
export const CRView = (props) => {
  try {
    var styleConstructor = [
      props.debug ? [styles.debug, { borderColor: props.debugColor }] : null,
      { backgroundColor: colors[props.backgroundColor] },
      props.style,
    ];
    var childrenConstructor = props.debug ? (
      <View style={[styles.tooltip, { backgroundColor: props.debugColor }]}>
        <CRText color={'white'}>{props.debugTooltipName}</CRText>
      </View>
    ) : null;
    return (
      <View style={styleConstructor} name={props.name}>
        {childrenConstructor}
        {props.children}
      </View>
    );
  } catch (error) {
    return <CRError component={'CRView'} error={error} />;
  }
};

CRView.propTypes = {
  ...model.types,
};

CRView.defaultProps = {
  ...model.default,
};

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import { StyleSheet } from "react-sketchapp";
import { colors } from "../../theme/colors";

export const styles = StyleSheet.create({
  base: {
    flexDirection: "column",
    justifyContent: "space-around",
    backgroundColor: colors.gray100,
    marginBottom: 120,
  },
  docTitle: {
    borderBottomColor: colors.gray200,
    borderBottomWidth: 1,
    paddingHorizontal: 10,
  },
  container: {},
  docFooter: {
    borderTopColor: colors.gray200,
    borderTopWidth: 1,
    paddingHorizontal: 10,
  },
});

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import * as React from "react";
import { Artboard } from "react-sketchapp";
//Mijo DS
import { CRText } from "../../components/CRText";
import { CRView } from "../../components/CRView";
import { CRContainer } from "../../components/CRContainer";
//Style
import { styles } from "./SCRArtboard.style";
import { CRSpace } from "../../components/CRSpace";

export const SCRArtboard = ({ children, style, doc, title, desc }) => {
  return (
    <Artboard style={[styles.base, style]} name={title ? title : "Artboard"}>
      <CRContainer>
        <CRView style={styles.docTitle} name={"Doc Title"}>
          {doc && title ? (
            <CRView name={"Doc Title"}>
              <CRSpace.Vertical variant={"large"} />
              <CRText variant={"title"}>{title ? title : ""}</CRText>
            </CRView>
          ) : null}
          {doc && desc ? (
            <CRView>
              <CRSpace.Vertical variantNumber={15} />
              <CRText color={"dark200"}>{desc ? desc : ""}</CRText>
              <CRSpace.Vertical variant={"large"} />
            </CRView>
          ) : (
            <CRView />
          )}
        </CRView>
        <CRView name={"Artboard Content"} style={styles.container}>
          <CRSpace.Vertical />
          {children}
          <CRSpace.Vertical />
        </CRView>
        {doc ? (
          <CRView name={"Doc Footer"}>
            <CRSpace.Vertical variant={"large"} />
            <CRView style={styles.docFooter}>
              <CRSpace.Vertical />
              <CRText color={"dark100"}>
                This sketch file was created under the MIJO framework.
              </CRText>
              <CRSpace.Vertical />
            </CRView>
          </CRView>
        ) : null}
      </CRContainer>
    </Artboard>
  );
};

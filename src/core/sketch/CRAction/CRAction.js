/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
import PropTypes from 'prop-types';
import { CRView } from '../CRView';
import { CRText } from '../CRText';

export const CRAction = ({
  children,
  value,
  variant,
  style,
  textColor,
  textAlign,
  textStyle,
  className,
  type,
  onPress,
  href,
  disabled,
  name,
  childrenName,
}) => {
  const actionTag = (
    <CRView key={variant} style={style} name={name}>
      <CRText
        color={textColor}
        align={textAlign}
        style={textStyle}
        name={childrenName}
      >
        {children ? children : value}
      </CRText>
    </CRView>
  );
  return actionTag;
};

CRAction.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  variant: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  textColor: PropTypes.string,
  textAlign: PropTypes.string,
  textStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  type: PropTypes.string,
  onPress: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  href: PropTypes.string,
  disabled: PropTypes.bool,
  name: PropTypes.string,
};

CRAction.defaultProps = {
  children: '',
  value: 'ACTION',
  variant: 'touchableOpacity',
  className: '',
  style: {},
  textColor: 'dark300',
  textAlign: 'left',
  textStyle: {},
  type: 'button',
  onPress: {},
  href: '/',
  disabled: false,
  name: 'CRAction',
};

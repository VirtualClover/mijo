/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React, { useState } from "react";
import PropTypes from "prop-types";
import { Image } from "react-sketchapp";
//Style
import { styles } from "./CRImg.style";

function useFallbackImg(img, fallback) {
  const [source, setImg] = useState(img);

  function onError(e) {
    // React bails out of hook renders if the state
    // is the same as the previous state, otherwise
    // fallback erroring out would cause an infinite loop
    setImg(fallback);
  }

  return { source, onError };
}

const placeholderDefault =
  "https://virtualclover.gitlab.io/mijo/master/static/media/placeholder.e9b1f94e.jpg";

export const CRImg = ({
  source,
  className,
  style,
  placeholderImg,
  resizeMode,
  alt,
  name,
}) => {
  try {
    var finalPlaceholder = placeholderImg ? placeholderImg : placeholderDefault;
    var styleConstructor = [styles.base, { resizeMode: resizeMode }, style];
    return (
      <Image
        style={styleConstructor}
        source={source ? source : finalPlaceholder}
        name={name}
      />
    );
  } catch (error) {
    console.log(error);
    return (
      <Image style={styles.base} source={placeholderDefault} name={"ERR"} />
    );
  }
};

CRImg.propTypes = {
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  source: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  placeholderImg: PropTypes.string,
  resizeMode: PropTypes.string,
  alt: PropTypes.string,
  name: PropTypes.string,
};

CRImg.defaultProps = {
  style: {},
  className: "",
  source: null,
  placeholderImg: "",
  resizeMode: "contain",
  alt: "",
  name: "CRImg",
};

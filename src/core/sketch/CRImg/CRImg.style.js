/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import { StyleSheet } from "react-sketchapp";
export const styles = StyleSheet.create({
  base: {
    minWidth: 30,
    minHeight: 30,
  },
});

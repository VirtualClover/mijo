/*

This file was created by Canasta Rosa 
under the MIT license.

*/
import React from 'react';
import iconConfig from '../../theme/assets/fonts/canasta_sans/selection.json';
import { CRText } from '../CRText';
import { model } from './CRIcon.model';

//First find the char code from the json
function getObject(data, value) {
  let objectOnArray = {};
  data.forEach(function (element) {
    for (let key in element) {
      if (element[key].name == value) objectOnArray = element[key];
    }
  });
  return objectOnArray;
}

export const CRIcon = (props) => {
  const iconObject = getObject(iconConfig.icons, props.variant);
  return (
    <CRText
      variant={'icon'}
      color={props.color}
      style={{ fontSize: props.size }}
      align={'center'}
      name={`CRIcon (${props.variant})`}
    >
      {String.fromCharCode(iconObject.code)}
    </CRText>
  );
};

CRIcon.propTypes = {
  ...model.types,
};

CRIcon.defaultProps = {
  ...model.default,
};

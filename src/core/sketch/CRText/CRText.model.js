/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    children: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.array,
      PropTypes.object,
    ]),
    variant: PropTypes.string,
    color: PropTypes.string,
    align: PropTypes.string,
    limit: PropTypes.number,
    className: PropTypes.string,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    name: PropTypes.string,
  },
  default: {
    children: 'SAMPLE TEXT',
    variant: 'paragraph',
    color: 'dark300',
    align: 'left',
    limit: 280,
    className: '',
    style: {},
    name: '',
  },
};

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
import { Text, TextStyles } from 'react-sketchapp';
//Model
import { model } from './CRText.model';
//TextStyles
import { typography } from '../../theme/typography';
//DS
import { colors } from '../../theme/colors';
import { CRError } from '../CRError';

//Load a and add the text styles to the file first
TextStyles.create(typography, {
  context: context,
  clearExistingStyles: true,
});

export const CRText = (props) => {
  try {
    if (props.children.length > props.limit) {
      var finalText = `${props.children.substring(0, props.limit)}...`;
    } else {
      finalText = props.children;
    }
    return (
      <Text
        style={[
          TextStyles.get(props.variant),
          { color: colors[props.color] },
          { textAlign: props.align },
          props.style,
        ]}
        name={
          props.name ? props.name : `CRText (${props.variant} / ${props.color})`
        }
      >
        {finalText}
      </Text>
    );
  } catch (error) {
    return <CRError component={'CRText'} error={error} />;
  }
};

CRText.propTypes = {
  ...model.types,
};

CRText.defaultProps = {
  ...model.default,
};

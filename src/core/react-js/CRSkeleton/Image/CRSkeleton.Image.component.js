/*

This file was created by Canasta Rosa 
under the MIT license.

*/
import React from 'react';
//MIJO
import { CRView } from '../../CRView';
import { CRError } from '../../CRError';
import { CRImg } from '../../CRImg';
//Style
import styles from '../CRSkeleton.style.scss';
//Model
import { model } from '../CRSkeleton.model';
import { imageModel } from './CRSkeleton.Image.model';

/**
 * MIJO'S DS skeleton component
 * @param {Object} props
 */
export const SkeletonImage = (props) => {
  try {
    return (
      <CRView className={props.className}>
        <CRView
          className={`cr__microInt--skeleton cr__microint--animatedBackground ${styles.image}`}
        >
          <CRImg source={props.placeholderImg} alt={props.alt} />
        </CRView>
      </CRView>
    );
  } catch (error) {
    return <CRError component={'CRSkeleton.Image'} error={error} />;
  }
};

SkeletonImage.propTypes = {
  ...model.types,
  ...imageModel.types,
};

SkeletonImage.defaultProps = {
  ...model.default,
  ...imageModel.default,
};

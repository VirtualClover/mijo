/*

This file was created by Canasta Rosa
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from 'prop-types';

export const imageModel = {
  types: {
    placeholderImg: PropTypes.string,
    alt: PropTypes.string,
  },
  default: {
    placeholderImg: '',
    alt: '',
  },
};

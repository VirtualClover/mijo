/*

This file was created by Canasta Rosa 
under the MIT license.

*/
import React from 'react';
//MIJO
import { CRView } from '../../CRView';
import { CRText } from '../../CRText';
import { CRError } from '../../CRError';
//Style
import styles from '../CRSkeleton.style.scss';
//Model
import { model } from '../CRSkeleton.model';
import { textModel } from './CRSkeleton.Text.model';

/**
 * MIJO'S DS skeleton component
 * @param {Object} props
 */
export const SkeletonText = (props) => {
  try {
    return (
      <CRView className={props.className}>
        <CRView
          className={`cr__microInt--skeleton cr__microint--animatedBackground ${styles.text}`}
        >
          <CRText variant={props.textVariant} color={'transparent'}>
            CR
          </CRText>
        </CRView>
      </CRView>
    );
  } catch (error) {
    return <CRError component={'CRSkeleton'} error={error} />;
  }
};

SkeletonText.propTypes = {
  ...model.types,
  ...textModel.types,
};

SkeletonText.defaultProps = {
  ...model.default,
  ...textModel.default,
};

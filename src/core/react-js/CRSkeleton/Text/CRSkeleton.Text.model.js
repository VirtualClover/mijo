/*

This file was created by Canasta Rosa
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from 'prop-types';

export const textModel = {
  types: {
    textVariant: PropTypes.string,
  },
  default: {
    textVariant: 'paragraph',
  },
};

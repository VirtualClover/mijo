/*

This file was created by Canasta Rosa 
under the MIT license.

*/
import React from 'react';
//MIJO
import { CRView } from '../CRView';
import { CRError } from '../CRError';
import { CRImg } from '../CRImg';
//Style
import styles from './CRSkeleton.style.scss';
//Model
import { model } from './CRSkeleton.model';
//Subcomponents
import { SkeletonImage } from './Image/CRSkeleton.Image.component';
import { SkeletonText } from './Text/CRSkeleton.Text.component';

/**
 * MIJO'S DS skeleton component
 * @param {Object} props
 */
export const CRSkeleton = (props) => {
  try {
    return (
      <CRView className={props.className}>
        <CRView
          className={`cr__microInt--skeleton cr__microint--animatedBackground ${styles.block}`}
        />
      </CRView>
    );
  } catch (error) {
    return <CRError component={'CRSkeleton'} error={error} />;
  }
};
//Subcomponents
CRSkeleton.Image = SkeletonImage;
CRSkeleton.Text = SkeletonText;
//Proptypes
CRSkeleton.propTypes = model.types;
CRSkeleton.defaultProps = model.default;

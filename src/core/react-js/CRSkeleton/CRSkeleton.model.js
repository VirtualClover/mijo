/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number,
      PropTypes.array,
    ]),
    className: PropTypes.string,
    opacity: PropTypes.number,
    variant: PropTypes.oneOf(['text', 'image', 'title', 'block']),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  },

  default: {
    style: {},
    className: '',
    opacity: 1,
    variant: 'block',
    height: 100,
    width: '100%',
  },
};

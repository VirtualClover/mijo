/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
//Style
import styles from './CRError.style.scss';

//Model
import { model } from './CRError.model';

export const CRError = (props) => {
  try {
    console.error(`${props.component}: ${props.error}`);
    return <div className={styles.error}>!ERR/{props.component}</div>;
  } catch (error) {
    console.error(`CRError: ${error}`);
    return <div className={styles.error}>!ERR/CRError</div>;
  }
};

CRError.propTypes = {
  ...model.types,
};

CRError.defaultProps = {
  ...model.default,
};

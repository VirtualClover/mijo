/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React, { useState } from 'react';
import { CRText } from '../CRText/CRText.component';
import { CRButton } from '../../components/CRButton';
import { model } from './CRtextInput.model';
import styles from './CRtextInput.style.scss';

export const CRTextInput = (props) => {
  //State
  const [actualBorderColor, onChangeBorder] = useState('gray200');
  const [error, setError] = useState(false);
  const [value, setValue] = useState(props.value);

  function handleOnChange(
    text,
    onChangeborderHook,
    setErrorHook,
    setValueHook,
    validation
  ) {
    setValueHook(text);
    setErrorHook(!validation);
  }
  return (
    <>
      <div
        className={`cr-bdrc-${actualBorderColor} ${styles.inputWrapper} ${
          error && 'cr-bdrc-red300'
        }`}
      >
        <input
          type="text"
          className={`cr__text--paragraph cr-txtc-dark300 ${styles.input}`}
          placeholder={props.placeholder}
          onFocus={() => onChangeBorder('blue200')}
          onBlur={() => onChangeBorder('gray200')}
          onChange={(text) =>
            handleOnChange(
              text,
              onChangeBorder,
              setError,
              setValue,
              props.validation
            )
          }
        />
        {props.iconVariant && (
          <div className={styles.iconWrapper}>
            <CRButton.Icon
              iconVariant={'search'}
              color={'white'}
              textColor={'dark300'}
              onPress={props.iconOnPress}
              className={styles.buttonStyle}
            />
          </div>
        )}
      </div>
      {error && (
        <CRText variant={'caption'} color={'main200'} align={'right'}>
          {props.errorMessage}
        </CRText>
      )}
    </>
  );
};

CRTextInput.defaultProps = model.default;

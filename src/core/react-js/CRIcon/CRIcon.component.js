/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
import { CRError } from '../CRError';

//Model
import { model } from './CRIcon.model';
/**
 * Icon component of the MIJO DS
 * @param {Object} props
 */
export const CRIcon = (props) => {
  try {
    const classConstructor = `cr--icon--${props.variant} cr-txtc-${props.color} ${props.className}`;
    return <i className={classConstructor} />;
  } catch (error) {
    return <CRError component={'CRIcon'} error={error} />;
  }
};

CRIcon.propTypes = {
  ...model.types,
};

CRIcon.defaultProps = {
  ...model.default,
};

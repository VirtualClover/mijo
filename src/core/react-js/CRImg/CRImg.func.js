/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** functionality of this component ************/

/**
 * Returns a fallback image(placeholder) in case the source image is not found.
 * @param {State Hook} src The state hook for the img source.
 * @param {function setStateHook(newState) {}} srcSethook The setState part of the react hook.
 * @param {String} fallback The placeholder in case the components returns an onError prop
 * @return An object containing a src prop and an onError prop.
 */
export function useFallbackImg(src, srcSethook, fallback) {
  function onError(e) {
    // React bails out of hook renders if the state
    // is the same as the previous state, otherwise
    // fallback erroring out would cause an infinite loop
    srcSethook(fallback);
  }

  return { src, onError };
}

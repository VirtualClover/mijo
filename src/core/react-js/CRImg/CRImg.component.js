/*

This file was created by Canasta Rosa 
under the MIT license.

*/
import React, { useState } from 'react';
//MIJO
import placeholderDefault from '../../theme/assets/images/placeholder/placeholder.jpg';
import { CRError } from '../CRError';
//Style
import styles from './CRImg.style.scss';
//Model
import { model } from './CRImg.model';
//Functionality
import { useFallbackImg } from './CRImg.func';
/**
 * Base image component of the MIJO DS
 * @param {Object} props
 */
export const CRImg = (props) => {
  try {
    const [src, setImg] = useState(props.source);
    var finalPlaceholder = props.placeholderImg
      ? props.placeholderImg
      : placeholderDefault;
    const srcProps = useFallbackImg(src, setImg, finalPlaceholder);
    var classConstructor = `${styles.base} ${props.className}`;
    return <img className={classConstructor} {...srcProps} alt={props.alt} />;
  } catch (error) {
    return <CRError component={'CRImg'} error={error} />;
  }
};

CRImg.propTypes = {
  ...model.types,
};

CRImg.defaultProps = {
  ...model.default,
};

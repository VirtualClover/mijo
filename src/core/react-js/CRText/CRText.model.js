/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from "prop-types";

export const model = {
  types: {
    children: PropTypes.node.isRequired,
    variant: PropTypes.oneOf([
      "title",
      "subtitle",
      "subtitle2",
      "subtitle3",
      "bold",
      "paragraph",
      "caption",
      "micro",
    ]).isRequired,
    align: PropTypes.string.isRequired,
    limit: PropTypes.number.isRequired,
    className: PropTypes.string.isRequired,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number,
      PropTypes.array,
    ]).isRequired,
  },

  default: {
    children: "SAMPLE TEXT",
    variant: "paragraph",
    color: "dark300",
    align: "left",
    limit: 280,
    className: "",
    style: {},
  },
};

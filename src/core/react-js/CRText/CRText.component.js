/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';

//Model
import { model } from './CRText.model';
import { CRError } from '../CRError';

/**
 * CRText
 * Base text component of the MIJO DS
 * @see <a href="https://virtualclover.gitlab.io/mijo/master/?path=/story/%F0%9F%A4%96-components-%F0%9F%96%8A%EF%B8%8F-typography-crtext--title">CRText documentation</a>
 */
export const CRText = (props) => {
  try {
    if (props.children.length > props.limit) {
      var finalText = props.children.substring(0, props.limit) + '...';
    } else {
      finalText = props.children;
    }

    const classConstructor = ` cr__text cr-txtc-${props.color} cr-txtaln-${props.align} ${props.className}`;

    const textTag = {
      title: (
        <h1
          key={props.variant}
          className={'cr__text--title' + classConstructor}
        >
          {finalText}
        </h1>
      ),
      subtitle: (
        <h2
          key={props.variant}
          className={'cr__text--subtitle' + classConstructor}
        >
          {finalText}
        </h2>
      ),
      subtitle2: (
        <h3
          key={props.variant}
          className={'cr__text--subtitle2' + classConstructor}
        >
          {finalText}
        </h3>
      ),
      subtitle3: (
        <h4
          key={props.variant}
          className={'cr__text--subtitle3' + classConstructor}
        >
          {finalText}
        </h4>
      ),
      bold: (
        <p key={props.variant} className={'cr__text--bold' + classConstructor}>
          {finalText}
        </p>
      ),
      paragraph: (
        <p
          key={props.variant}
          className={'cr__text--paragraph' + classConstructor}
        >
          {finalText}
        </p>
      ),
      caption: (
        <p
          key={props.variant}
          className={'cr__text--caption' + classConstructor}
        >
          {finalText}
        </p>
      ),
      micro: (
        <p key={props.variant} className={'cr__text--micro' + classConstructor}>
          {finalText}
        </p>
      ),
    };

    return textTag[props.variant];
  } catch (error) {
    return <CRError component={'CRText'} error={error} />;
  }
};

CRText.propTypes = {
  ...model.types,
};

CRText.defaultProps = {
  ...model.default,
};

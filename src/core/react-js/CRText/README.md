# CRText

This is the main text component of MIJO DS.

## Import

React Native  
`import {CRText} from '@mijo/react-native/CRText'`

## Props

| Prop    | Type   | About                                  | Accepts                                                   | Default      |
| ------- | ------ | -------------------------------------- | --------------------------------------------------------- | ------------ |
| variant | style  | Specifies the variant of the text      | All typography styles                                     | paragraph    |
| color   | string | Specifies the color of the text        | Any color                                                 | colorDark300 |
| align   | string | Specifies the align of the text        | <ul><li>'left'</li><li>'right'</li><li>'center'</li></ul> | 'left'       |
| limit   | number | Specifies the limit length of the text | Any number                                                | 280          |

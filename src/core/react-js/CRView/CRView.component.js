/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
//Style
import styles from './CRView.style.scss';

//Model
import { model } from './CRView.model';
import { CRError } from '../CRError';

export const CRView = (props) => {
  try {
    var classConstructor = `${props.debug ? styles.debug : ''} cr-bgc-${
      props.backgroundColor
    } ${props.className}`;
    var classConstructorToolTip = `${props.debug ? styles.tooltip : ''} ${
      props.debugTooltipClass
    }`;
    var childrenConstructor = props.debug ? (
      <div className={classConstructorToolTip}>{props.debugTooltipName}</div>
    ) : (
      ''
    );
    return (
      <div className={classConstructor}>
        {childrenConstructor}
        {props.children}
      </div>
    );
  } catch (error) {
    return <CRError component={'CRView'} error={error} />;
  }
};

CRView.propTypes = {
  ...model.types,
};

CRView.defaultProps = {
  ...model.default,
};

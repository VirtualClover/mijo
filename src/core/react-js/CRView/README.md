# CRView

This is the main text component of MIJO DS.

## Import

`import {CRView} from '@mijo/components/CRView'`

## Props

### React JS

| Prop              | Type    | About                               | Accepts      | Default  |
| ----------------- | ------- | ----------------------------------- | ------------ | -------- |
| debug             | boolean | Puts the component in UI debug mode | True / False | false    |
| className         | string  | Adds a class to the component       | Any string   | ''       |
| debugTooltipName  | string  | Changes the debug tooltip name      | Any string   | 'CRView' |
| debugTooltipClass | string  | Adds a class to the tooltip         | Any string   | ''       |

### React Native

| Prop              | Type           | About                               | Accepts                  | Default  |
| ----------------- | -------------- | ----------------------------------- | ------------------------ | -------- |
| debug             | boolean        | Puts the component in UI debug mode | True / False             | false    |
| style             | object / array | Adds a styling to the component     | Any React native styling | null     |
| debugTooltipName  | string         | Changes the debug tooltip name      | Any string               | 'CRView' |
| debugTooltipColor | string         | Changes the debug color             | Any string               | 'green'  |

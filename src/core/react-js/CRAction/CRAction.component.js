/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from 'react';
import '../../theme/theme.scss';
//MIJO
import { CRError } from '../CRError';
//Model
import { model } from './CRAction.model';
/**
 * Base action component of the MIJO DS
 * @param {Object} props
 */
export const CRAction = (props) => {
  try {
    const actionTag = {
      link: [
        <a
          key={props.variant}
          className={props.className}
          href={props.href}
          onClick={props.onPress}
        >
          {props.children ? props.children : props.value}
        </a>,
      ],
      a: [
        <a
          key={props.variant}
          href={props.href}
          className={props.className}
          onClick={props.nPress}
        >
          {props.children ? props.children : props.value}
        </a>,
      ],
      button: [
        <button
          key={props.variant}
          className={props.className}
          type={props.type}
          disabled={props.disabled}
          onClick={props.onPress}
          value={props.value}
        >
          {props.children ? props.children : props.value}
        </button>,
      ],
    };

    return actionTag[props.variant];
  } catch (error) {
    return <CRError component={'CRAction'} error={error} />;
  }
};

CRAction.propTypes = {
  ...model.types,
};

CRAction.defaultProps = {
  ...model.default,
};

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from "prop-types";

export const model = {
  types: {
    children: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    variant: PropTypes.string,
    className: PropTypes.string,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number,
      PropTypes.array,
    ]),
    textColor: PropTypes.string,
    textAlign: PropTypes.string,
    textStyle: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number,
      PropTypes.array,
    ]),
    type: PropTypes.string,
    onPress: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
    href: PropTypes.string,
    disabled: PropTypes.bool,
  },
  default: {
    children: "",
    value: "ACTION",
    variant: "button",
    className: "",
    style: {},
    textColor: "dark300",
    textAlign: "left",
    textStyle: {},
    type: "button",
    onPress: () => console.log("onPress Check"),
    href: "/",
    disabled: false,
  },
};

<img src='https://virtualclover.gitlab.io/mijo/master/docs-images/cover.jpg' />

# MIJO core

This is the heart of our design system.

## Quick start

1. Install the base package depending on the platform.

### ReactJS

```bash
yarn add @mijo/reactjs
```

### React Native

```bash
yarn add @mijo/react-native
```

2. Install the components

```bash
yarn add @mijo/components
```

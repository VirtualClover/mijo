# CDS Theme

This the heart of the Canasta Design System, it contains all the core variables and assets that [Canasta Rosa](https://canastarosa.com/) uses.
This is a dependency for both ReactJS and React Native versions of the CDS, but if you want to use it as a standalone for JS and SASS variables:

## Install

```bash
npm i @canastarosa/ds-theme

```

/*

This file was created by Canasta Rosa 
under the MIT license.

*/
import { colors } from '../../colors';

export const typography = {
  icon: {
    fontFamily: 'canasta-sans',
    fontSize: 16,
  },
  //<CRText type="pageTitle" color=colors.main100/>
  title: {
    // Main title of pages the most weight over all.
    textAlign: 'left',
    fontSize: 32,
    fontFamily: 'Sarabun-SemiBold',
    color: colors.dark300,
  },

  subtitle: {
    // As the name implies this style is for subtitles.
    textAlign: 'left',
    fontSize: 26,
    fontFamily: 'Sarabun-SemiBold',
    color: colors.dark300,
  },

  subtitle2: {
    // A subtitle but....smaller..
    textAlign: 'left',
    fontSize: 22,
    fontFamily: 'Sarabun-SemiBold',
    color: colors.dark300,
  },

  subtitle3: {
    // The smallest of the subtitles.
    textAlign: 'left',
    fontSize: 18,
    fontFamily: 'Sarabun-SemiBold',
    color: colors.dark300,
  },

  button: {
    // The text style for buttons and such.
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'Sarabun-Regular',
    color: colors.dark300,
  },

  paragraph: {
    // As the name implies this one is for paraghraps.
    textAlign: 'left',
    fontSize: 16,
    fontFamily: 'Sarabun-Regular',
    color: colors.dark300,
  },

  bold: {
    // This is the bold version of the paragrhaph, to highlight something in a text.
    textAlign: 'left',
    fontSize: 16,
    fontFamily: 'Sarabun-SemiBold',
    color: colors.dark300,
  },

  caption: {
    // As the name implies this one is for paraghraps.
    textAlign: 'left',
    fontSize: 13,
    fontFamily: 'Sarabun-Regular',
    color: colors.dark300,
  },
  micro: {
    // As the name implies this one is for paraghraps.
    textAlign: 'left',
    fontSize: 10,
    fontFamily: 'Sarabun-Regular',
    color: colors.dark300,
  },
};

export const textStyles = {
  icon: typography.icon,
  title: typography.title,
  subtitle: typography.subtitle,
  subtitle2: typography.subtitle2,
  subtitle3: typography.subtitle3,
  bold: typography.bold,
  paragraph: typography.paragraph,
  caption: typography.caption,
  micro: typography.micro,
};

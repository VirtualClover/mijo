/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/**
 * Converts hex to rgba
 * @param {string} hexcode
 * @param {number} opacity
 * @returns A rgba style for js styling
 */
export default function rgba(hexcode, opacity) {
  const values = [
    hexcode.substring(1, 3),
    hexcode.substring(3, 5),
    hexcode.substring(5, 7),
  ].map((string) => parseInt(string, 16));
  return `rgba(${values[0]}, ${values[1]}, ${values[2]}, ${opacity})`;
}

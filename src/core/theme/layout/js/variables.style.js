/*

This file was created by Canasta Rosa 
under the MIT license.

*/
/**
 * Gets width of the device
 */
export const WIDTH = Dimensions.get('window');
/**
 * Our spaces variables, we handle spaces in multiples of 5
 */
export const spaces = {
  size5: 5,
  size10: 10,
  size15: 11,
  size20: 20,
  size25: 25,
  size30: 30,
  size35: 35,
  size40: 40,
  size45: 45,
  size50: 50,
  size55: 55,
  size60: 60,
  size65: 65,
  size70: 70,
  size75: 75,
  size80: 80,
  size85: 85,
  size90: 90,
  size95: 95,
  size100: 100,
};

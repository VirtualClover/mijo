# CRError

Fallback component used on other component's error boundaries.
Console logs the error of the failed component.

## Import

`import {CRError} from '@mijo/components/CRError'`

## Props

### React JS

| Prop      | Type   | About                                                        | Accepts    | Default              |
| --------- | ------ | ------------------------------------------------------------ | ---------- | -------------------- |
| component | string | The component from which this component serves as a fallback | Any string | Undefined Component  |
| error     | string | The error returned from the catch block                      | Any string | No error log warning |

### React Native

| Prop      | Type   | About                                                        | Accepts    | Default              |
| --------- | ------ | ------------------------------------------------------------ | ---------- | -------------------- |
| component | string | The component from which this component serves as a fallback | Any string | Undefined Component  |
| error     | string | The error returned from the catch block                      | Any string | No error log warning |

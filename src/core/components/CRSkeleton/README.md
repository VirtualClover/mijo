# CRSkeleton

This is the base skeleton component for loading stages.
Every variant (except image) always occupies the **100% of it's container width**.

## Import

`import {CRSkeleton} from '@mijo/components/CRSkeleton'`

## Props

### React JS

| Prop      | Type   | About                                          | Accepts                                                          | Default |
| --------- | ------ | ---------------------------------------------- | ---------------------------------------------------------------- | ------- |
| alt       | string | Adds an alt attribute if the variant is image. | Any string                                                       | ''      |
| className | string | Adds a class to the component                  | Any string                                                       | ''      |
| variant   | string | Specifies the variant of the component         | <ul><li>block</li><li>title</li><li>text</li><li>image</li></ul> | block   |

### React Native

| Prop    | Type            | About                                    | Accepts                                                          | Default |
| ------- | --------------- | ---------------------------------------- | ---------------------------------------------------------------- | ------- |
| height  | number / string | Overwrites the height of the component.  | Any number or percentages from 0 to 100                          | 0       |
| opacity | number          | Overwrites the opacity of the component. | 0-1                                                              | 0       |
| style   | object / array  | Adds a styling to the component          | Any React native styling                                         | null    |
| variant | string          | Specifies the variant of the component   | <ul><li>block</li><li>title</li><li>text</li><li>image</li></ul> | block   |
| width   | number / string | Overwrites the width of the component.   | Any number or percentages from 0 to 100                          | 0       |

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from "prop-types";

export const model = {
  types: {
    name: PropTypes.string.isRequired,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number,
      PropTypes.array,
    ]),
    justifyContent: PropTypes.oneOf([
      "flex-start",
      "flex-end",
      "center",
      "space-between",
      "space-around",
      "space-evenly",
    ]),
    flexDirection: PropTypes.oneOf(["row", "row-reversed"]),
    alignItems: PropTypes.oneOf([
      "stretch",
      "flex-start",
      "flex-end",
      "center",
      "baseline",
    ]),
    className: PropTypes.string,
    debug: PropTypes.bool,
  },
  default: {
    name: "CRRow",
    style: {},
    className: "",
    debug: false,
    justifyContent: "flex-start",
    flexDirection: "row",
    alignItems: "center",
  },
};

# CRRow

This the row component component of MIJO DS. It uses flexbox to function both in ReactJS and React Native.

## Import

`import {CRow} from '@mijo/components/CRow'`

## Props

### React JS

| Prop      | Type    | About                               | Accepts      | Default |
| --------- | ------- | ----------------------------------- | ------------ | ------- |
| debug     | boolean | Puts the component in UI debug mode | True / False | false   |
| className | string  | Adds a class to the component       | Any string   | ''      |

### React Native

| Prop           | Type           | About                                  | Accepts                  | Default  |
| -------------- | -------------- | -------------------------------------- | ------------------------ | -------- |
| alignItems     | string         | Overwrites the alignItems property     | Any string               | 'CRView' |
| debug          | boolean        | Puts the component in UI debug mode    | True / False             | false    |
| flexDirection  | string         | Overwrites the flexDirection property  | Any string               | 'CRView' |
| justifyContent | string         | Overwrites the justifyContent property | Any string               | 'green'  |
| style          | object / array | Adds a styling to the component        | Any React native styling | null     |

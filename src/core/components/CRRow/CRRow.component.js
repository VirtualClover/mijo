/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from "react";
import { styles } from "./CRRow.style";
import { cssBase, cssDebug, cssDebugTooltip } from "./CRRow.style.scss";
//Model
import { model } from "./CRRow.model";
//Mijo DS
import { CRView } from "../CRView";

export const CRRow = (props) => {
  var classConstructor = `${cssBase} ${props.debug ? cssDebug : ""} ${
    props.className
  }`;
  var styleConstructor = [
    styles.base,
    props.style,
    {
      justifyContent: props.justifyContent,
      alignItems: props.alignItems,
      flexDirection: props.flexDirection,
    },
  ];
  return (
    <CRView
      name={props.name}
      style={styleConstructor}
      className={classConstructor}
      debugTooltipName={"CRRow"}
      debugTooltipClass={cssDebugTooltip}
      debugColor={"orangered"}
      debug={props.debug}
    >
      {props.children}
    </CRView>
  );
};

CRRow.propTypes = {
  ...model.types,
};

CRRow.defaultProps = {
  ...model.default,
};

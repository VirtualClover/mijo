/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from "prop-types";

export const model = {
  types: {
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number,
      PropTypes.array,
    ]),
    className: PropTypes.string,
    debug: PropTypes.bool,
  },
  default: {
    style: {},
    className: "",
    debug: false,
  },
};

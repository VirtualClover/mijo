# CRContainer

This the main container component of MIJO DS.
When creating the content of a page, this is the base element we need to lay down.
By default this component has the gray100 as the color of the background.
It always occupies 100% of the width of its container.

## Import

`import {CRContainer} from '@mijo/components/CRContainer'`

## Props

### React JS

| Prop      | Type    | About                               | Accepts      | Default |
| --------- | ------- | ----------------------------------- | ------------ | ------- |
| debug     | boolean | Puts the component in UI debug mode | True / False | false   |
| className | string  | Adds a class to the component       | Any string   | ''      |

### React Native

| Prop  | Type           | About                               | Accepts                  | Default |
| ----- | -------------- | ----------------------------------- | ------------------------ | ------- |
| debug | boolean        | Puts the component in UI debug mode | True / False             | false   |
| style | object / array | Adds a styling to the component     | Any React native styling | null    |

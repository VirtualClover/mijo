/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
import { styles } from './CRContainer.style';
import { cssBase, cssDebug, cssDebugTooltip } from './CRContainer.style.scss';
//Model
import { model } from './CRContainer.model';
//Mijo DS
import { CRView } from '../CRView';

export const CRContainer = (props) => {
  var classConstructor = `${cssBase} ${props.debug ? cssDebug : ''} ${
    props.className
  }`;
  var styleConstructor = [styles.base, props.style];
  return (
    <CRView
      name={`CRContainer`}
      style={styleConstructor}
      className={classConstructor}
      debugTooltipName={'CRContainer'}
      debugTooltipClass={cssDebugTooltip}
      debugColor={'hotpink'}
      debug={props.debug}
      backgroundColor={'gray100'}
    >
      {props.children}
    </CRView>
  );
};

CRContainer.propTypes = {
  ...model.types,
};

CRContainer.defaultProps = {
  ...model.default,
};

# CRImg

This is the main text component of MIJO DS.

## Import

`import {CRImg} from '@mijo/components/CRImg`

## Props

### React JS

| Prop           | Type   | About                                    | Accepts    | Default |
| -------------- | ------ | ---------------------------------------- | ---------- | ------- |
| alt            | string | Specifies the alt attribute of the image | Any String | ''      |
| className      | string | Adds a class to the component            | Any string | ''      |
| placeholderImg | string | Overwrites the default placeholder image | Any string | ''      |
| source         | string | Specifies the source of the image        | Any string | ''      |

### React Native

| Prop           | Type           | About                                                                                         | Accepts                  | Default |
| -------------- | -------------- | --------------------------------------------------------------------------------------------- | ------------------------ | ------- |
| placeholderImg | string         | Overwrites the default placeholder image, needs URI paring if the sources is an external link | Any string               | ''      |
| source         | string         | Specifies the source of the image, needs URI paring if the sources is an external link        | Any string               | ''      |
| style          | object / array | Adds a styling to the component                                                               | Any React native styling | null    |

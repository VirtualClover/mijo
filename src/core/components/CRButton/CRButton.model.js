/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    children: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onPress: PropTypes.func,
    color: PropTypes.string,
    textColor: PropTypes.string,
    textAlign: PropTypes.string,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    disabled: PropTypes.bool,
    iconVariant: PropTypes.string,
    iconSize: PropTypes.number,
    iconClassName: PropTypes.string,
  },
  default: {
    children: '',
    value: 'BUTTON',
    actionVariant: 'button',
    onPress: () => console.log('Button Press'),
    color: 'main300',
    textColor: 'white',
    textAlign: 'center',
    limit: 25,
    style: {},
    textStyle: {},
    width: 150,
    disabled: false,
  },
  ghost: {
    textColor: 'main300',
  },
  icon: {
    iconVariant: 'cross',
    iconSize: 16,
    iconClassName: '',
    width: 'auto',
  },
};

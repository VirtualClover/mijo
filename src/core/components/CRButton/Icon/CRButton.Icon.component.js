/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
import { styles } from '../CRButton.style';
import { cssBase, cssIcon } from '../CRButton.style.scss';
//MIJO
import { CRAction } from '../../CRAction';
import { CRError } from '../../CRError';
import { CRIcon } from '../../CRIcon';
import { colors } from '../../../theme/colors';
// Model
import { model } from '../CRButton.model';

export const Icon = (props) => {
  try {
    if (props.disabled) {
      var classConstructor = `${cssBase} ${cssIcon} cr__text--paragraph cr-txtc-dark100 cr-bgc-gray300 cr-bdrc-gray300 ${props.className}`;
      var styleConstructor = [
        styles.base,
        styles.icon,
        { backgroundColor: colors.gray300 },
        { borderColor: colors.gray300 },
        { width: props.width },
        props.style,
      ];
      var textStyleConstructor = 'dark100';
    } else {
      var classConstructor = `${cssBase} ${cssIcon} cr__text--paragraph cr-txtc-${props.textColor} cr-bgc-${props.color} cr-bdrc-${props.color} ${props.className}`;
      var styleConstructor = [
        styles.base,
        styles.icon,
        { backgroundColor: colors[props.color] },
        { borderColor: colors[props.color] },
        { width: props.width },
        props.style,
      ];
      var textStyleConstructor = props.textColor;
    }
    return (
      <CRAction
        style={styleConstructor}
        className={classConstructor}
        variant={props.actionVariant}
        onPress={props.onPress}
        textColor={textStyleConstructor}
        textAlign={props.textAlign}
        textStyle={props.textStyle}
        disabled={props.disabled}
        value={props.value}
        childrenName={`CRIcon (${props.iconVariant})`}
      >
        <CRIcon
          variant={props.iconVariant}
          className={props.iconClassName}
          size={props.iconSize}
          color={props.textColor}
        />
      </CRAction>
    );
  } catch (error) {
    return <CRError component={'CRButton.Icon'} error={error} />;
  }
};

Icon.propTypes = {
  ...model.types,
};

Icon.defaultProps = {
  ...model.default,
  ...model.icon,
};

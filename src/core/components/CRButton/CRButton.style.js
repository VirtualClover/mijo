/*

This file was created by Canasta Rosa
under the MIT license.

*/

import { StyleSheet } from 'react-native';
import { colors } from '../../theme/colors';

export const styles = StyleSheet.create({
  base: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 3,
    borderWidth: 0.65,
    width: 150,
  },

  icon: {
    borderRadius: '50%',
    alignItems: 'center',
    paddingVertical: 5.5,
    paddingHorizontal: 5.5,
    justifyContent: 'center',
  },

  error: {
    borderColor: colors.red300,
    backgroundColor: colors.red300,
  },
});

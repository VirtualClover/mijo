/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
import { styles } from '../CRButton.style';
import { cssBase } from '../CRButton.style.scss';
//MIJO
import { CRAction } from '../../CRAction';
import { CRError } from '../../CRError';
import { colors } from '../../../theme/colors';
// Model
import { model } from '../CRButton.model';

export const Ghost = (props) => {
  try {
    if (props.disabled) {
      var classConstructor = `${cssBase} cr__text--paragraph cr-txtc-dark100 cr-bgc-transparent cr-bdrc-gray300 ${props.className}`;
      var styleConstructor = [
        styles.base,
        { backgroundColor: null },
        { borderColor: colors.gray300 },
        { width: props.width },
        props.style,
      ];
      var textStyleConstructor = 'dark100';
    } else {
      var classConstructor = `${cssBase} cr__text--paragraph cr-txtc-${props.textColor} cr-bgc-transparent cr-bdrc-${props.color} ${props.className}`;
      var styleConstructor = [
        styles.base,
        { backgroundColor: null },
        { borderColor: colors[props.color] },
        { width: props.width },
        props.style,
      ];
      var textStyleConstructor = props.textColor;
    }
    return (
      <CRAction
        style={styleConstructor}
        className={classConstructor}
        variant={props.actionVariant}
        onPress={props.onPress}
        textColor={textStyleConstructor}
        textAlign={props.textAlign}
        textStyle={props.textStyle}
        disabled={props.disabled}
        value={props.value}
      >
        {props.children}
      </CRAction>
    );
  } catch (error) {
    return <CRError component={'CRButton.Ghost'} error={error} />;
  }
};

Ghost.propTypes = {
  ...model.types,
};

Ghost.defaultProps = {
  ...model.default,
  ...model.ghost,
};

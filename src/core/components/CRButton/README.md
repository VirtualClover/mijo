# CRButton

This is the main button of our design system.

## Import

`import {CRButton} from '@mijo/components/CRButton'`

## Props

| Prop      | Type     | About                                                    | Accepts                                                                           | Default    |
| --------- | -------- | -------------------------------------------------------- | --------------------------------------------------------------------------------- | ---------- |
| variant   | string   | Specifies the variant of the button                      | <ul><li>'pink'</li><li>'pinkGhost'</li><li>'white'</li><li>'whiteGhost'</li></ul> | 'pink'     |
| textColor | string   | Specifies the color of the text                          | Any color                                                                         | colorWhite |
| attitude  | string   | Specifies the status of the button                       | <ul><li>'disabled'</li></ul>                                                      | ''         |
| limit     | number   | Specifies the limit length of the text inside the button | Any number                                                                        | 25         |
| onPress   | function | The function to do when pressed                          | Any function                                                                      | null       |

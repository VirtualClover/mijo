/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
import { styles } from './CRButton.style';
import { cssBase } from './CRButton.style.scss';
//MIJO
import { CRAction } from '../CRAction';
import { colors } from '../../theme/colors';
// Model
import { model } from './CRButton.model';
//Variants
import { Ghost } from './Ghost/CRButton.Ghost.component';
import { Icon } from './Icon/CRButton.Icon.component';

export const CRButton = (props) => {
  try {
    if (props.disabled) {
      var classConstructor = `${cssBase} cr__text--paragraph cr-txtc-dark100 cr-bgc-gray300 cr-bdrc-gray300 ${props.className}`;
      var styleConstructor = [
        styles.base,
        { backgroundColor: colors.gray300 },
        { borderColor: colors.gray300 },
        { width: props.width },
        props.style,
      ];
      var textStyleConstructor = 'dark100';
    } else {
      var classConstructor = `${cssBase} cr__text--paragraph cr-txtc-${props.textColor} cr-bgc-${props.color} cr-bdrc-${props.color} ${props.className}`;
      var styleConstructor = [
        styles.base,
        { backgroundColor: colors[props.color] },
        { borderColor: colors[props.color] },
        { width: props.width },
        props.style,
      ];
      var textStyleConstructor = props.textColor;
    }
    return (
      <CRAction
        style={styleConstructor}
        className={classConstructor}
        variant={props.actionVariant}
        onPress={props.onPress}
        textColor={textStyleConstructor}
        textAlign={props.textAlign}
        textStyle={props.textStyle}
        disabled={props.disabled}
        value={props.value}
      >
        {props.children}
      </CRAction>
    );
  } catch (error) {
    console.error(error);
    return <CRError component={'CRButton.Main'} error={error} />;
  }
};
//Variants
CRButton.Ghost = Ghost;
CRButton.Icon = Icon;

CRButton.propTypes = {
  ...model.types,
};

CRButton.defaultProps = {
  ...model.default,
};

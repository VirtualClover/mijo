# CRText

This the main container text component of MIJO DS.
It contains the diferent text styles of the mijo framework.

## Import

`import {CRText} from '@mijo/components/CRText'`

## Props

### React JS

| Prop      | Type   | About                                  | Accepts                                             | Default   |
| --------- | ------ | -------------------------------------- | --------------------------------------------------- | --------- |
| align     | string | Specifies the align of the text        | <ul><li>left</li><li>center</li><li>right</li></ul> | left      |
| className | string | Adds a class to the component          | Any string                                          | ''        |
| limit     | number | Specifies the limit length of the text | Any number                                          | 280       |
| variant   | string | Specifies the text style               | Any text styles                                     | paragraph |

### React Native

| Prop    | Type           | About                                  | Accepts                                             | Default   |
| ------- | -------------- | -------------------------------------- | --------------------------------------------------- | --------- |
| align   | string         | Specifies the align of the text        | <ul><li>left</li><li>center</li><li>right</li></ul> | left      |
| limit   | number         | Specifies the limit length of the text | Any number                                          | 280       |
| style   | object / array | Adds a styling to the component        | Any React native styling                            | null      |
| variant | string         | Specifies the text style               | Any text styles                                     | paragraph |

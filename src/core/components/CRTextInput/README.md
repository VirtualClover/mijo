# CRTextInput

This is the base text input of our design system.

## Import

`import {CRTextInput} from '@mijo/components/CRTextInput'`

## Props

### React JS

| Prop         | Type          | About                                                      | Accepts              | Default         |
| ------------ | ------------- | ---------------------------------------------------------- | -------------------- | --------------- |
| placeholder  | string        | Overwrites the default placeholder                         | Any String           | Placeholder     |
| value        | string/number | The starter value of the input                             | Any string or number | ''              |
| errorMessage | string        | The error message when an error is detonaded               | Any string           | 'ERROR MESSAGE' |
| iconVariant  | string        | The icon appearing on the left                             | an icon name         | ''              |
| validation   | bool          | Specifies wether the current value is valid or not         | true/false           | false           |
| onChangeText | function      | Specifies what happens when value inside the input changes | any function         | ()=>{}          |

## Usage

We recommend importing the input and using hooks to store the value whe the text changes using the **onTextChange** prop and validating it if it's correct or not, for example:

```
import {CRTextInput} from '@mijo/components/CRTextInput';

...

export const input = () => {

  const [userName, setUserName] = useState('');
  const [hasError, setHasError] = useSate(false);

  function handleUserInput(value){
      setUserName(value);
      !userName && hasError(true);

  };

  return(

      <>
      <CRTexInput
      placeholder={'Username'}
      onChangeText={(text)=> handleUserInput(text)}
      validation={!hasError}
      errorMessage={'This field must not be empty!'}
      />
      </>
  )

};
```

/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from "prop-types";

export const model = {
  types: {
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number,
      PropTypes.array,
    ]),
    className: PropTypes.string,
    variant: PropTypes.oneOf(["full", "default"]).isRequired,
    name: PropTypes.string,
    debug: PropTypes.bool,
    title: PropTypes.string,
  },
  default: {
    style: {},
    className: "",
    full: false,
    name: "CRSection",
    debug: false,
    title: null,
    variant: "default",
  },
};

/*

This file was created by Canasta Rosa
under the MIT license.

*/

import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  base: {
    marginHorizontal: 10,
  },
  full: {
    marginHorizontal: 0,
  },
});

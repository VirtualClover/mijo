/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
import { styles } from './CRSection.style';
import {
  cssSection,
  cssDebug,
  cssTooltip,
  cssFull,
} from './CRSection.style.scss';
//Model
import { model } from './CRSection.model';
//Mijo DS
import { CRView } from '../CRView';
import { CRText } from '../CRText';
import { CRSpace } from '../CRSpace';

export const CRSection = ({
  children,
  style,
  className,
  variant,
  name,
  debug,
  title,
  desc,
}) => {
  var classConstructor = `${cssSection} ${variant == 'full' ? cssFull : ''} ${
    debug ? cssDebug : ''
  } cr-mrgn-top-size10 cr-mrgn-btm-size10 ${className}`;
  var styleConstructor = [
    styles.base,
    variant == 'full' ? styles.full : null,
    style,
  ];
  return (
    <CRView
      name={`CRSection (${variant})`}
      style={styleConstructor}
      className={classConstructor}
      debugTooltipName={'CRSection'}
      debugColor={'purple'}
      debug={debug}
      debugTooltipClass={cssTooltip}
    >
      <CRSpace.Vertical variant={'small'} />
      {title ? <CRText variant={'subtitle'}>{title}</CRText> : null}
      {desc ? (
        <CRView name="Section Description">
          <CRSpace.Vertical variant={'small'} />
          <CRText variant={'paragraph'} color={'dark200'}>
            {desc}
          </CRText>
        </CRView>
      ) : null}
      {title || desc ? <CRSpace.Vertical variantNumber={15} /> : null}
      {children}
      <CRSpace.Vertical variant={'small'} />
    </CRView>
  );
};

CRSection.propTypes = {
  ...model.types,
};

CRSection.defaultProps = {
  ...model.default,
};

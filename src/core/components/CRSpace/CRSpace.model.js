/*

This file was created by Canasta Rosa 
under the MIT license.

*/

/********** Model of this component ************/
import PropTypes from "prop-types";

export const model = {
  types: {
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number,
      PropTypes.array,
    ]),
    className: PropTypes.string,
    variant: PropTypes.oneOf([
      "xSmall",
      "small",
      "medium",
      "large",
      "xLarge",
      "xxLarge",
    ]).isRequired,
    variantNumber: PropTypes.number,
    debug: PropTypes.bool,
  },
  default: {
    style: {},
    className: "",
    variant: "medium",
    variantNumber: 0,
    debug: false,
  },
  predefined: {
    xSmall: 5,
    small: 10,
    medium: 20,
    large: 30,
    xLarge: 40,
    xxLarge: 50,
  },
};

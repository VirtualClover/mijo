/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
//MIJO
import { CRView } from '../../CRView';
import { CRText } from '../../CRText';
//Style
import { styles } from '../CRSpace.style';
import { cssSpace, cssDebug } from '../CRSpace.style.scss';
//Model
import { model } from '../CRSpace.model';

export const Horizontal = (props) => {
  var sizeSelector = props.variantNumber
    ? props.variantNumber
    : model.predefined[props.variant];
  var classConstructor = `${cssSpace} ${
    props.debug ? cssDebug : ''
  } cr-wdt-size${sizeSelector} ${props.className}`;
  var styleConstructor = [
    styles.horizontal,
    { width: sizeSelector },
    props.debug ? styles.debug : null,
    props.style,
  ];
  return (
    <CRView
      name={`CRSpace Horizontal (${sizeSelector})`}
      style={styleConstructor}
      className={classConstructor}
    >
      {props.debug ? (
        <CRView>
          <CRText variant={'paragraph'} color={'white'} align={'center'}>
            CRSpace Horizontal ({sizeSelector})
          </CRText>
        </CRView>
      ) : null}
    </CRView>
  );
};

Horizontal.propTypes = {
  ...model.types,
};

Horizontal.defaultProps = {
  ...model.default,
};

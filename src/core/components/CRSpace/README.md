# CRSpace

This component creates predefined blank spaces, useful when you are trying to keep certain rythm.
Spaces can be horizontal or vertical. When a space is vertical it occupies 100% of the width and viceversa.

## Import

`import {CRSpace} from '@mijo/components/CRSpace';`

## Use

The CRSpace is a parent component that encapsulates two subcomponents vertical and horizontal, **it is important to specify which you want to use since not doing so will cause an error.**

### Vertical

`<CRSpace.Vertical />`

### Horizontal

`<CRSpace.Horizontal />`

## Props

### React JS

| Prop          | Type    | About                                                         | Accepts                                                                                            | Default  |
| ------------- | ------- | ------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- | -------- |
| debug         | boolean | Puts the component in UI debug mode                           | True / False                                                                                       | false    |
| className     | string  | Adds a class to the component                                 | Any string                                                                                         | ''       |
| variant       | string  | Specifies the size of the space using predefined sizes        | <ul><li>xSmall</li><li>small</li><li>medium</li><li>large</li><li>xLarge</li><li>xxLarge</li></ul> | 'medium' |
| variantNumber | number  | Specifies the size of the space, overwrittes the variant prop | Any multiple of 5 up to 100                                                                        | 0        |

### React Native

| Prop          | Type           | About                                                         | Accepts                                                                                            | Default  |
| ------------- | -------------- | ------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- | -------- |
| debug         | boolean        | Puts the component in UI debug mode                           | True / False                                                                                       | false    |
| style         | object / array | Adds a styling to the component                               | Any React native styling                                                                           | null     |
| variant       | string         | Specifies the size of the space using predefined sizes        | <ul><li>xSmall</li><li>small</li><li>medium</li><li>large</li><li>xLarge</li><li>xxLarge</li></ul> | 'medium' |
| variantNumber | number         | Specifies the size of the space, overwrittes the variant prop | Any multiple of 5 up to 100                                                                        | 0        |

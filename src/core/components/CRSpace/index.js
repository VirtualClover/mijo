/*

This file was created by Canasta Rosa 
under the MIT license.

*/
import { Vertical } from './Vertical/CRSpace.Vertical.component';
import { Horizontal } from './Horizontal/CRSpace.Horizontal.component';

export const CRSpace = Vertical;
//Variants
CRSpace.Vertical = Vertical;
CRSpace.Horizontal = Horizontal;

/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
//MIJO
import { CRView } from '../../CRView';
import { CRText } from '../../CRText';
//Style
import { styles } from '../CRSpace.style';
import { cssSpace, cssDebug } from '../CRSpace.style.scss';
//Model
import { model } from '../CRSpace.model';

export const Vertical = (props) => {
  var sizeSelector = props.variantNumber
    ? props.variantNumber
    : model.predefined[props.variant];
  var classConstructor = `${cssSpace} ${
    props.debug ? cssDebug : ''
  } cr-hgt-size${sizeSelector} ${props.className}`;
  var styleConstructor = [
    { height: sizeSelector },
    props.debug ? styles.debug : null,
    props.style,
  ];
  return (
    <CRView
      name={`CRSpace Vertical (${sizeSelector})`}
      style={styleConstructor}
      className={classConstructor}
    >
      {props.debug ? (
        <CRView>
          <CRText variant={'paragraph'} color={'white'} align={'center'}>
            CRSpace Vertical ({sizeSelector})
          </CRText>
        </CRView>
      ) : null}
    </CRView>
  );
};

Vertical.propTypes = {
  ...model.types,
};

Vertical.defaultProps = {
  ...model.default,
};

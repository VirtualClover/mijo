/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from "react";
//Mijo DS
import { CRView } from "../../CRView";
import { CRImg } from "../../CRImg";
import { styles } from "./../CRLogo.style";
import {
  cssBase,
  cssFullWrapper,
  cssNegative,
  cssImageBase,
} from "../CRLogo.style.scss";

//Style

export const Negative = () => {
  const classContructor = `${cssBase} ${cssFullWrapper}`;
  const imageClassContructor = `${cssImageBase} ${cssNegative}`;
  return (
    <CRView
      name={"CRLogo (Negative)"}
      style={[styles.base, styles.fullWrapper]}
      className={classContructor}
    >
      <CRImg
        source={
          "https://virtualclover.gitlab.io/mijo/master/static/media/logos/logoMap.png"
        }
        resizeMode={"repeat"}
        style={[styles.mapImage, styles.negative]}
        className={imageClassContructor}
      />
    </CRView>
  );
};

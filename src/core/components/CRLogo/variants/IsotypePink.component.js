/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from "react";
//Mijo DS
import { CRView } from "../../CRView";
import { CRImg } from "../../CRImg";
import { styles } from "./../CRLogo.style";

//Style

export const IsotypePink = () => {
  return (
    <CRView
      name={"CRLogo (Isotype Pink)"}
      style={[styles.base, styles.isotypeWrapper]}
    >
      <CRImg
        source={
          "https://virtualclover.gitlab.io/mijo/master/static/media/logos/logoMap.png"
        }
        resizeMode={"repeat"}
        style={[styles.mapImage, styles.isotypePink]}
      />
    </CRView>
  );
};

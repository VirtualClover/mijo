/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import React from "react";
//Mijo DS
import { CRView } from "../../CRView";
import { CRImg } from "../../CRImg";
import { styles } from "./../CRLogo.style";
import {
  cssBase,
  cssIsotypeWrapper,
  cssIsotypeNegative,
  cssImageBase,
} from "../CRLogo.style.scss";

//Style

export const IsotypeNegative = () => {
  const classContructor = `${cssBase} ${cssIsotypeWrapper}`;
  const imageClassContructor = `${cssImageBase} ${cssIsotypeNegative}`;
  return (
    <CRView
      name={"CRLogo (Isotype Negative)"}
      style={[styles.base, styles.isotypeWrapper]}
      className={classContructor}
    >
      <CRImg
        source={
          "https://virtualclover.gitlab.io/mijo/master/static/media/logos/logoMap.png"
        }
        resizeMode={"repeat"}
        style={[styles.mapImage, styles.isotypeNegative]}
        className={imageClassContructor}
      />
    </CRView>
  );
};

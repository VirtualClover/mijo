/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import { StyleSheet } from "react-primitives";

export const styles = StyleSheet.create({
  base: {
    overflow: "hidden",
    backgroundColor: "transparent", //Added so the overflow works
    margin: 10,
  },
  fullWrapper: {
    width: 185,
    height: 30,
  },
  isotypeWrapper: {
    width: 31,
    height: 30,
  },
  mapImage: {
    width: 185,
    height: 183,
  },
  negative: {
    top: -31,
  },
  isotypeNegative: {
    top: -62,
    left: -31,
  },
  isotypeWhite: {
    top: -62,
    left: -62,
  },
  isotypePink: {
    top: -62,
    left: -93,
  },
});

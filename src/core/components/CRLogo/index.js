import { Main } from "./variants/Main.component";
import { Isotype } from "./variants/Isotype.component";
import { Negative } from "./variants/Negative.component";
import { IsotypeNegative } from "./variants/IsotypeNegative.component";
import { IsotypeWhite } from "./variants/IsotypeWhite.component";
import { IsotypePink } from "./variants/IsotypePink.component";
export const CRLogo = {
  Main,
  Isotype,
  Negative,
  IsotypeNegative,
  IsotypeWhite,
  IsotypePink,
};

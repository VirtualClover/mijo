# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.12] - 2020-06-15

### Added

- CRButton

### Fixed

- CRtext import issues

## [0.0.11] - 2020-06-15

### Fixed

- Extended artifact life span of the plugin per tag

## [0.0.10] - 2020-06-12

### Fixed

- Gitlab CI/CD

## [0.0.9] - 2020-06-11

### Added

- Plugin as an artifact with gitlab CI

## [0.0.8] - 2020-06-11

### Fixed

- Release scripts

## [0.0.7] - 2020-06-11

### Added

- NPM CI integration

## [0.0.6] - 2020-05-27

### Added

- Uploading scripts
- Changed /designSystem to /core

## [0.0.5] - 2020-05-26

### Added

- CRText
- Theme
- Sketch-ReactApp integration

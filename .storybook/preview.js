export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  layout: "centered",
  options: {
    storySort: {
      order: [
        "🔎 Overview",
        ["👋 Welcome", "🏛️ Principles"],
        "🧱 Foundations",
        "🎨 UI Design",
        [
          "🚀 Getting started",
          "🔃 Design lifecycle",
          "📏 Design guidelines",
          "🔌 Sketch plugins",
        ],
        "🤖 Components",
        "Stories",
        ["About Stories"],
        "MIJO Maintenance",
        ["📥 Installing the project", "📚 Pillar libraries"],
      ],
    },
  },
};

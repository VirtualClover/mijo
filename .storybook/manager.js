import { addons } from "@storybook/addons";
import { default as mijoTheme } from "./mijoTheme";

addons.setConfig({
  theme: mijoTheme,
});

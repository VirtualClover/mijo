import { create } from "@storybook/theming/create";
import { colors } from "../src/core/theme/colors";
import logo from "../public/static/media/logos/mijo_framework_horizontal.jpg";

export default create({
  base: "light",

  colorPrimary: colors.main100,
  colorSecondary: colors.dark300,

  // UI
  appBg: "white",
  appContentBg: colors.white,
  appBorderColor: colors.gray200,
  appBorderRadius: 3,

  // Typography
  fontBase: '"Sarabun", sans-serif',
  fontCode: "monospace",

  // Text colors
  textColor: colors.dark300,
  textInverseColor: colors.white,

  // Toolbar default and active colors
  barTextColor: colors.dark300,
  barSelectedColor: colors.turquiose300,
  barBg: colors.gray100,

  // Form colors
  inputBg: colors.gray100,
  inputBorder: colors.gray200,
  inputTextColor: colors.dark300,
  inputBorderRadius: 3,

  brandTitle: "MIJO DS",
  brandUrl: "/",
  brandImage: logo,
});

const path = require("path");
const custom = require("./webpack.config.js");

module.exports = {
  webpackFinal: (config) => {
    //config.module.rules.push(...custom.rules); //pushed to prevent "config.rules is not iterable"
    config.module.rules.push(
      {
        test: /\.scss$/,
        exclude: /\.style\.s[ac]ss$/,
        use: ["style-loader", "css-loader?modules=false", "sass-loader"],
        include: path.resolve(__dirname, "../"),
      },
      {
        test: /\.style\.s[ac]ss$/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              modules: {
                localIdentName: "crh-[hash:base64:5]",
              },
            },
          },
          "sass-loader",
        ],
        include: path.resolve(__dirname, "../"),
      }
    );
    return {
      ...config,
      resolve: { ...config.resolve, alias: custom.resolve.alias },
      plugins: [...config.plugins, ...custom.plugins],
    };
  },
  stories: ["../src/**/*.stories.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],
  addons: ["@storybook/addon-links", "@storybook/addon-essentials"],
};

const CopyPlugin = require("copy-webpack-plugin");
const path = require("path");
// Changes import from native to primitives or sketch and web rendering
module.exports = {
  resolve: {
    extensions: [".js", ".stories.js"],
    alias: {
      "react-native$": "react-native-web",
    },
  },
  plugins: [
    //Ensures the images for docs can be accesed from markdown and storybook
    new CopyPlugin({
      patterns: [
        {
          from: "src/**/docs-images/**/*.(png|jpg|gif)",
          to: "docs-images",
          flatten: true,
        },
      ],
    }),
  ],
};

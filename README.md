<img src="https://virtualclover.gitlab.io/mijo/master/docs-images/cover.jpg" class="img-flat">

# Welcome to MIJO

This is Canasta Rosa's design framework. It has been built to support our teams of designers and developers across Canasta Rosa in creating best in class digital products and experiences.

This framework consists of: our design principles, design and development guidelines, methodologies, styleguides, and working code. It is maintained by our core design system managers and continually improved by our amazing community of contributors.

# MIJO local enviroment install

## Installing the project

1. Git clone the project  
   `git clone git@gitlab.com:VirtualClover/mijo.git`

2. Install the dependecies  
   `yarn run quick-start`

3. Run the project  
    You have various options from here on now.

   - If you want to start storybook  
     `yarn run storybook`

   - If you want to test the sketch plugin  
     Create a new sketch file and then the in the terminal  
     `yarn run render`
